# Block League Tutorial
Block League tutorial, inspired by the S4 League one. Players have a maximum of 15 minutes to complete it; otherwise, they're teleported outside to avoid blocking the arena

## Instructions
The map takes an area of about 180x40x180 nodes.
1. Do `//pos1` to set the origin where you're standing. It'll be the bottom left corner of the map region, which will be pasted towards positive coordinates (+X/+Y/+Z).
2. Do `//load tutorial_map` to paste the map
3. Create an arena (e.g. `/arenas create block_league tutorial`)
4. Enter the arena editor
5. Go to Players and disable teams
6. Set min and max players to 1
7. Go to Spawners and set the spawn point standing on the node indicated [in the picture](https://i.imgur.com/3aSHtCk.png). **If you set the spawner incorrectly, the tutorial won't work!**
8. Go to Settings -> Arena properties and set `arena_mode` to `0`
9. Optionally go to Customise -> Background music and put a track. Suggested track: [Non-Euclidean Geometry](https://yewtu.be/watch?v=HAv0r4_l0gk) by Punch Deck (CC BY 4.0)
10. Enable and leave the editor (remember to set the entrance)

### Updating the map
##### Server admins
Just in case the map needs an update (e.g. found some holes, improved some areas), this is the position you should stand to load the schematic again: https://i.imgur.com/meEZn6a.mp4

##### Builders
1. Select as `pos1` the node that server admins use and as `pos2` something around 40-50 nodes above that, on the opposite corner of the map
2. Run `//save tutorial_map`; confirm with `//y`
3. Move the file created with the same name in `<worldfolder>/schems` into `<modfolder>/bl_tutorial/schems`
