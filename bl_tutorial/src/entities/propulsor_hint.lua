local hint = {
  initial_properties = {
    physical = false,
    visual = "upright_sprite",
    visual_size = {x = 0.5, y = 1, z = 0.5},
    textures = {"bl_tutorial_propulsor_waypoint.png"}
  }
}

core.register_entity("bl_tutorial:propulsor_hint", hint)