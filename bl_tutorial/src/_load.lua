local S = core.get_translator("bl_tutorial")



local function load_schematic()
  local src_dir = core.get_modpath("bl_tutorial") .. "/schems"
  local wrld_dir = core.get_worldpath() .. "/schems"
  local mod_schem = io.open(src_dir .. "/tutorial_map.we", "r")
  local mod_schem_size = mod_schem:seek("end")
  local wrld_schem = io.open(wrld_dir .. "/tutorial_map.we")

  if not wrld_schem or mod_schem_size ~= wrld_schem:seek("end") then
    core.cpdir(src_dir, wrld_dir)

    if wrld_schem then
      io.close(wrld_schem)
      core.log("action", "[BL Tutorial] Schematic of the tutorial map was outdated. Updated to the new version")
    end
  end

  io.close(mod_schem)
end

load_schematic()



----------------------------------------------
------------------AUDIO_LIB-------------------
----------------------------------------------

audio_lib.register_sound("sfx", "bl_tutorial_obstacle_death", S("Obstacle breaks"))



----------------------------------------------
--------------ACHIEVEMENTS_LIB----------------
----------------------------------------------

achvmt_lib.register_achievement("block_league:first_steps", {
  title = S("First steps"),
  description = S("Complete the tutorial"),
  image = "bl_ach_tutorial.png",
  tier = "Bronze"
})
