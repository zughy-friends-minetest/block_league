local function get_row_length() end
local function calc_action_offset() end



local ROW1_HEIGHT_TXT = 24
local ROW2_HEIGHT_TXT = 76
local ROW3_HEIGHT_TXT = 128
local ROW1_HEIGHT_IMG = ROW1_HEIGHT_TXT -14
local ROW2_HEIGHT_IMG = ROW2_HEIGHT_TXT -14
local ROW3_HEIGHT_IMG = ROW3_HEIGHT_TXT -14
local ROW1_OFFSET_BG = ROW1_HEIGHT_TXT -7
local ROW2_OFFSET_BG = ROW2_HEIGHT_TXT -7
local ROW3_OFFSET_BG = ROW3_HEIGHT_TXT -7
local BG = "bl_util_blacksquare.png^[opacity:180]"
local BG_SCALE = { x = 10, y = 2}
local ICON_SCALE = { x = 3, y = 3}



function block_league.HUD_log_create(p_name)
  Panel:new("bl_log", {
    player = p_name,
    bg = "",
    position = { x = 1, y = 0 },
    alignment = { x = -1, y = 1 },

    sub_img_elems = {
      action_1_bg = {
        alignment = {x = -1, y = 1},
        offset = { x = 0, y = ROW1_OFFSET_BG},
        scale = BG_SCALE,
        z_index = -1
      },
      action_2_bg = {
        alignment = {x = -1, y = 1},
        offset = { x = 0, y = ROW2_OFFSET_BG},
        scale = BG_SCALE,
        z_index = -1,
      },
      action_3_bg = {
        alignment = {x = -1, y = 1},
        offset = { x = 0, y = ROW3_OFFSET_BG},
        scale = BG_SCALE,
        z_index = -1,
      },
      action_1 = {
        alignment = {x = -1, y = 1},
        scale = ICON_SCALE
      },
      action_2 = {
        alignment = {x = -1, y = 1},
        scale = ICON_SCALE
      },
      action_3 = {
        alignment = {x = -1, y = 1},
        scale = ICON_SCALE
      },
    },
    sub_txt_elems = {
      executor_1 = {
        alignment = {x = -1, y = 1},
        style = 4
      },
      executor_2 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW2_HEIGHT_TXT },
        style = 4
      },
      executor_3 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW3_HEIGHT_TXT },
        style = 4
      },
      receiver_1 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW1_HEIGHT_TXT },
        style = 4
      },
      receiver_2 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW2_HEIGHT_TXT },
        style = 4
      },
      receiver_3 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW3_HEIGHT_TXT },
        style = 4
      },
    }
  })
end



-- executor, receiver: tabella = entità, stringa = gioc.
-- receiver: stringa vuota = no receiver
-- assister stringa a priori. Passo `@` come primo carattere per indicare che è un'entità (per le traduzioni)
function block_league.HUD_log_update(arena, action_img, executor, receiver, assister)
  local is_exec_player = type(executor) == "string"
  local is_rec_player = type(receiver) == "string"
  local exec_teamID, rec_teamID -- TODO: in futuro usare rec_teamID x controllare fuoco amico (es. bombe)

  if is_exec_player then
    exec_teamID = arena.players[executor].teamID
  else
    exec_teamID = executor._teamID -- al momento dà per scontato che non possa essere `nil`. Questo sarà da rivedere se verranno mai implementate entità neutre
    executor = executor._name
  end

  if is_rec_player then
    if receiver ~= "" then
      rec_teamID = arena.players[receiver].teamID
    end
  else
    rec_teamID = receiver._teamID -- idem come sopra
    receiver = receiver._name
  end

  for psp_name, _ in pairs(arena.players_and_spectators) do
    local executor_color, receiver_color

    -- giocanti
    if arena.players[psp_name] then
      if arena.players[psp_name].teamID == exec_teamID then
        executor_color = "0xabf877"
        receiver_color = "0xff8e8e"
      else
        executor_color = "0xff8e8e"
        receiver_color = "0xabf877"
      end
    -- spettanti
    else
      if exec_teamID == 1 then
        executor_color = "0xf2a05b"
        receiver_color = "0x32d6e9"
      else
        executor_color = "0x32d6e9"
        receiver_color = "0xf2a05b"
      end
    end

    -- x calcolare distanze mi servono stringhe tradotte in caso di nomi entità
    local p_lang = core.get_player_information(psp_name).lang_code
    local executor_tr = is_exec_player and executor or core.get_translated_string(p_lang, executor)
    local receiver_tr = is_rec_player and receiver or core.get_translated_string(p_lang, receiver)
    local assister_tr = (assister and assister:find("@")) and core.get_translated_string(p_lang, string.sub(assister, 2)) or assister
    local executors = assister and (assister_tr .. " + " .. executor_tr) or executor_tr

    local panel = panel_lib.get_panel(psp_name, "bl_log")
    local rec1_offset = calc_action_offset(panel.receiver_2.text)
    local rec2_offset = calc_action_offset(panel.receiver_3.text)
    local rec3_offset = calc_action_offset(receiver_tr)

    panel:update(nil,

    -- icone
    {
      action_1_bg = {
        scale = { x = get_row_length(psp_name, panel.receiver_2.text, panel.executor_2.text) / 14, y = 2},
        text = panel.action_2.text ~= "" and BG or ""
      },
      action_2_bg = {
        scale = { x = get_row_length(psp_name, panel.receiver_3.text, panel.executor_3.text) / 14, y = 2},
        text = panel.action_3.text ~= "" and BG or ""
      },
      action_3_bg = {
        scale = { x = get_row_length(psp_name, receiver_tr, executors) / 14, y = 2},
        text = BG
      },
      action_1 = {
        offset = { x = rec1_offset, y = ROW1_HEIGHT_IMG },
        text = panel.action_2.text
      },
      action_2 = {
        offset = { x = rec2_offset, y = ROW2_HEIGHT_IMG },
        text = panel.action_3.text
      },
      action_3 = {
        offset = { x = rec3_offset, y = ROW3_HEIGHT_IMG },
        text = action_img
      }
    },

    -- testo
    {
      executor_1 = {
        offset = { x = rec1_offset - 60, y = ROW1_HEIGHT_TXT },
        number = panel.executor_2.number,
        text = panel.executor_2.text
      },
      executor_2 = {
        offset = { x = rec2_offset - 60, y = ROW2_HEIGHT_TXT },
        number = panel.executor_3.number,
        text = panel.executor_3.text
      },
      executor_3 = {
        offset = { x = rec3_offset - 60, y = ROW3_HEIGHT_TXT },
        number = executor_color,
        text = executors
      },
      receiver_1 = {
        number = panel.receiver_2.number,
        text = panel.receiver_2.text
      },
      receiver_2 = {
        number = panel.receiver_3.number,
        text = panel.receiver_3.text
      },
      receiver_3 = {
        number = receiver_color,
        text = receiver_tr
      }
    })

  end
end



function block_league.HUD_log_clear(arena)
  for pl_name, _ in pairs(arena.players_and_spectators) do
    local panel = panel_lib.get_panel(pl_name, "bl_log")

    panel:update(nil,
    -- icons
    {
      action_1_bg = {text = ""},
      action_2_bg = {text = ""},
      action_3_bg = {text = ""},
      action_1 = {text = ""},
      action_2 = {text = ""},
      action_3 = {text = ""}
    },

    -- txt
    {
      executor_1 = {text = ""},
      executor_2 = {text = ""},
      executor_3 = {text = ""},
      receiver_1 = {text = ""},
      receiver_2 = {text = ""},
      receiver_3 = {text = ""}
    })
  end
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function get_row_length(p_name, receiver, executor)
  local icon_size = 32
  local padding = executor and 48 or 35
  local char_amount = string.len(receiver) + string.len(executor or "")
  local distance = 7.3
  local window_info = core.get_player_window_information(p_name)
  local window_size_x = window_info and window_info.size.x or 1920 -- clients <5.7 won't return window_info, avoid crash
  local screen_x_ratio = 1920 / window_size_x -- text isn't affected by screen size, scale is. This fixes it

  return (-icon_size - padding - (distance * char_amount)) * screen_x_ratio
end



function calc_action_offset(receiver)
  return -27 - (10 * string.len(receiver))
end
