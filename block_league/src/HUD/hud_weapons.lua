-- copia dell'equipaggiamento per alleggerire calcoli su HUD_weapons_update.
-- Prob è inutile far funzione solo per svuotare la voce dellə giocante, tanto viene
-- ricreata ogni volta che si entra in partita
local p_equip = {}                -- KEY: p_name; VALUE: {equip1, 2, 3}



function block_league.HUD_weapons_create(p_name, is_spectator, mode)
  local inv = core.get_player_by_name(p_name):get_inventory()
  local sub_img_elems = {}
  local sub_txt_elems = {}
  local offset_x = -90
  local offset_y = is_spectator and -160 or -125

  -- armi
  for i = 1, 3 do
    local w_name = ""
    local weapon = {}

    -- dato che quando entrano, lɜ spettanti non sono ancora dentro alla spettatore,
    -- inizializzo stringhe vuote che verranno colmate dal primo on_change_spectated_target
    -- (che è istantaneo)
    if not is_spectator then
      w_name = inv:get_stack("main", i):get_name() ~= "" and inv:get_stack("main", i):get_name() or ""
      weapon = core.registered_nodes[w_name]
    end

    sub_img_elems[i .. "_bg"] = {
      scale     = { x = 2, y = 2 },
      offset    = { x = offset_x, y = offset_y },
      alignment = { x = 0, y = 1 },
      text      = "bl_hud_bullets_bg.png",
      z_index   = 0
    }

    if weapon then
      sub_img_elems[i .. "_icon"] = {
        scale     = { x = 2, y = 2 },
        offset    = { x = offset_x, y = offset_y },
        alignment = { x = -1, y = 1 },
        text      = weapon.inventory_image,
        z_index   = 1
      }

      sub_txt_elems[i .. "_magazine_txt"] = {
        alignment = { x = 0, y = 1 },
        offset    = { x = offset_x + 30, y = offset_y + 6 },
        text      = weapon.magazine or "",
        z_index   = 1
      }
    end

    offset_x = offset_x + 90
  end

  -- creo pannello
  Panel:new("bl_weapons", {
    player = p_name,
    bg = "",
    position = { x = 0.5, y = 1 },
    alignment = { x = 0, y = 0 },
    title = "",

    sub_img_elems = sub_img_elems,
    sub_txt_elems = sub_txt_elems
  })

  -- 1. anche per spettanti, in caso entrino poi in partita
  -- 2. se è nel tutorial, deve ragionare con smg, nil, nil
  p_equip[p_name] = mode == 0 and {"block_league:smg"} or table.copy(block_league.get_equipped_weapons(p_name))
end



function block_league.HUD_weapons_update(arena, p_name, w_name, is_reloading)
  local equip = p_equip[p_name]
  local slot_id

  for i = 1, 3 do
    if equip and equip[i] == w_name then
      slot_id = i
    end
  end

  local weapon = core.registered_nodes[w_name]
  local is_melee = not weapon.magazine
  local current_magazine = is_melee and "" or weapons_lib.get_magazine(p_name, w_name)
  local bg_pic = ""

  if is_reloading or current_magazine == 0 then
    bg_pic = "bl_hud_bullets_bg_reload.png"
  elseif not is_melee and current_magazine <= weapon.magazine/3 then
    bg_pic = "bl_hud_bullets_bg_low.png"
  else
    bg_pic = "bl_hud_bullets_bg.png"
  end

  local panel = panel_lib.get_panel(p_name, "bl_weapons")
  local icon = weapon.inventory_image -- potrei aggiungere parametro `is_spec_rotating` per non calcolarla su ogni sparo,
                                      -- ma penso complicherebbe comprensione del codice (con if nel for dellɜ spettanti)
  panel:update(nil,
    {[slot_id .. "_magazine_txt"] = { text = current_magazine }},
    {[slot_id .. "_bg"] = { text = bg_pic }}
  )

  for sp_name, _ in pairs(arena_lib.get_player_spectators(p_name)) do
    local panel_sp = panel_lib.get_panel(sp_name, "bl_weapons")

    panel_sp:update(nil,
      {[slot_id .. "_magazine_txt"] = { text = current_magazine }},
      {[slot_id .. "_bg"] = { text = bg_pic }, [slot_id .. "_icon"] = {text = icon}}
    )
  end
end



-- funzione chiamata esclusivamente quando spettanti cambiano giocante seguitə e
-- questə ha un buco nella casella dove lə giocante precedente invece aveva un'arma.
-- In altre parole, svuoto la casella
function block_league.HUD_weapons_update_empty(p_name, empty_id)
  for sp_name, _ in pairs(arena_lib.get_player_spectators(p_name)) do
    local panel_sp = panel_lib.get_panel(sp_name, "bl_weapons")

    panel_sp:update(nil,
      {[empty_id .. "_magazine_txt"] = { text = "" }},
      {[empty_id .. "_bg"] = { text = "bl_hud_bullets_bg.png" }, [empty_id .. "_icon"] = { text = ""}}
    )
  end
end
