function block_league.HUD_skill_create(p_name, is_spectator)

  local skill, x_offset

  if is_spectator then
    skill  = ""
    x_offset = 168
  else
    skill  = "bl_skill_" ..  block_league.get_equipped_skill(p_name):sub(14, -1) .. ".png"
    x_offset = 145
  end

  Panel:new("bl_skill", {
    player = p_name,
    bg = skill,
    position = { x = 0.5, y = 1 },
    alignment = { x = 0, y = 0 },
    offset = { x = x_offset, y = -30 },
    bg_scale = { x = 3, y = 3 },
    title = ""
  })
end



-- solo spettatori per ora
function block_league.HUD_skill_update(sp_name)
  local panel = panel_lib.get_panel(sp_name, "bl_skill")
  local skill = block_league.get_equipped_skill(arena_lib.get_spectated_target(sp_name).name)

  panel:update({bg = "bl_skill_" .. skill:sub(14, -1) .. ".png"})
end
