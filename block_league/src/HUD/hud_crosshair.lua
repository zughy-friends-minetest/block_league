-- usato in caso armi abbiano un mirino più elaborato (come kunai che indicava anche
-- quando era pronto per teletrasporto)
function block_league.HUD_crosshair_create(p_name)
  Panel:new("bl_crosshair", {
    player = p_name,
    bg = "",
    bg_scale = {x = 2, y = 2},
    position = { x = 0.5, y = 0.5 },
    alignment = {x = 0, y = 0}
  })

  Panel:new("bl_crosshair_overlay", {
    player = p_name,
    bg = "",
    bg_scale = {x = 2, y = 2},
    position = { x = 0.5, y = 0.5 },
    alignment = {x = 0, y = 0},
    visible = false
  })
end



function block_league.HUD_crosshair_update(p_name, w_name, is_reloading)
  local panel = panel_lib.get_panel(p_name, "bl_crosshair")
  local weap = core.registered_nodes[w_name] or core.registered_tools[w_name]
  local col = ""

  if is_reloading == true or (is_reloading == nil and string.find(panel:get_info().bg.text, "multiply")) then
    col = "^[multiply:#ff492c"
  end

  panel:update({bg = weap.crosshair .. col})
end