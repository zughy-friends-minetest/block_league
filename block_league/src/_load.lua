local S = core.get_translator("block_league")

----------------------------------------------
------------------AUDIO_LIB-------------------
----------------------------------------------
audio_lib.register_type("block_league", "voices", {mod = S("Block League"), type = S("Voices")})

audio_lib.register_sound("sfx", "bl_bomb_ignite", S("Bomb ignites"), {ephemeral = false}, 1.89)
audio_lib.register_sound("sfx", "bl_bomb_explosion", S("Bomb explodes"), {gain = 2, pitch = 0.7})
audio_lib.register_sound("sfx", "bl_bomb_throw", S("Bomb thrown"), {pitch = 0.85})
audio_lib.register_sound("sfx", "bl_crowd_cheer", S("Crowd cheering"), nil, 2.95)
audio_lib.register_sound("sfx", "bl_crowd_ohno", S("Sad crowd"), nil, 2.12)
audio_lib.register_sound("sfx", "bl_death", S("Heartbeat beeps"), {gain = 0.8}, 2.44)
audio_lib.register_sound("sfx", "bl_gui_equip_confirm", S("Confirmation sound"))
audio_lib.register_sound("sfx", "bl_hit", S("Hits target"))
audio_lib.register_sound("sfx", "bl_hit_deny", S("Hits target (deny)"))
audio_lib.register_sound("sfx", "bl_jingle_victory", S("Victory jingle"), nil, 3.43)
audio_lib.register_sound("sfx", "bl_jingle_defeat", S("Sad trombone"), nil, 3.88)
audio_lib.register_sound("sfx", "bl_kill", S("Ding!"))
audio_lib.register_sound("sfx", "bl_pixelgun_reload", S("Pixelgun reloads"), {ephemeral = false}, 4.11)
audio_lib.register_sound("sfx", "bl_pixelgun_shoot", S("Pixelgun shoots"))
audio_lib.register_sound("sfx", "bl_propulsor_bounce", S("Boing"), nil, 1.10)
audio_lib.register_sound("sfx", "bl_peacifier_reload", S("Peacifier reloads"), {ephemeral = false}, 2.02)
audio_lib.register_sound("sfx", "bl_peacifier_shoot", S("Peacifier shoots"))
audio_lib.register_sound("sfx", "bl_sentrygun_death", S("Sentry explodes"))
audio_lib.register_sound("sfx", "bl_sentrygun_shoot", S("Sentry shoots"))
audio_lib.register_sound("sfx", "bl_sentrygun_spawn", S("Sentry spawns"))
audio_lib.register_sound("sfx", "bl_smg_reload", S("SMG reloads"), {ephemeral = false}, 2.17)
audio_lib.register_sound("sfx", "bl_smg_shoot", S("SMG shoots"))
audio_lib.register_sound("sfx", "bl_smg_shoot2", S("SMG shoots (2)"))
audio_lib.register_sound("sfx", "bl_sword_hit", S("Sword swings"))
audio_lib.register_sound("sfx", "bl_sword_dash", S("Dashes"))
audio_lib.register_sound("sfx", "bl_timer", S("Timer ticks"))
audio_lib.register_sound("voices", "bl_voice_critical", S("\"Critical!\""), {ephemeral = false, cant_overlap = true}, 1.051)
audio_lib.register_sound("voices", "bl_voice_fight", S("\"Fight!\""))
audio_lib.register_sound("voices", "bl_voice_ball_reset", S("\"Ball reset\""), nil, 1.21)
audio_lib.register_sound("voices", "bl_voice_countdown_3", S("\"3, 2, 1...\""), {ephemeral = false}, 2.96)

----------------------------------------------
-----------------WEAPONS_LIB------------------
----------------------------------------------

weapons_lib.register_mod("block_league", {
    SHOOT_SPEED_MULTIPLIER = block_league.SHOOT_SPEED_MULTIPLIER
})