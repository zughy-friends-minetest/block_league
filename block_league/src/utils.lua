function block_league.is_in_the_air(obj_ref)
  local obj_pos = obj_ref:get_pos()
  local node_beneath = vector.new(obj_pos.x, obj_pos.y - 0.4, obj_pos.z)
  local is_in_the_air = core.get_node(node_beneath).name == "air"

  return is_in_the_air
end



function block_league.play_sound_to_player(p_name, sound)
  weapons_lib.play_sound(sound, p_name)
end



function block_league.remove_player_entities(p_entities) -- p_stats.entities
  for _, obj in pairs(p_entities) do
    local entity = obj:get_luaentity()
    if entity then
      if not entity._is_dying then
        if entity._kill then
          entity:_kill()
        else
          obj:remove()
        end
      end
    end
  end

  p_entities = {}
end



function block_league.is_entity_in_player_team(p_name, entity)
  if not entity._teamID then return end

  local teamID = arena_lib.get_teamID(p_name)

  if not teamID then return end

  return entity._teamID == teamID
end



function block_league.print_error(p_name, msg)
	core.chat_send_player(p_name, core.colorize("#e6482e", arena_lib.mods["block_league"].prefix .. msg))
end



function block_league.print(p_name, msg)
	core.chat_send_player(p_name, arena_lib.mods["block_league"].prefix .. msg)
end