local S = core.get_translator("block_league")


-- TODO: can't implement a proper shield due to the fact that raycasts don't work
-- well with children. I tried to create a dummy shield that followed the real one,
-- actually handling the collisions, but the 1 step delay makes this solution
-- unfeasible (the player is not covered most of the time). It needs
-- https://github.com/minetest/minetest/issues/10304
skills.register_skill("block_league:shield", {
  name = S("Shield"),
  icon = "bl_skill_shield.png",
  _profile_description = S("Summons a shield in front of you that assorbs bullets at the cost of stamina") .. "</style>\n\n"
    .. S("Expose yourself without any risk"),

  attachments = {
    entities = {{ name = "block_league:shield", pos = vector.new(0,12,6) }}
  },

  hud = {{
      name = "overlay",
      type = "image",
      text = "bl_skill_shield_texture.png^[opacity:128",
      scale = {x = 200, y = 200},
      z_index = -200
    }},

  on_start = function(self)
    local player = self.player
    -- TODO: controlla se può eseguirla
    -- TODO: abbassa stamina con variabile
  end,

  on_stop = function(self)
    -- TODO: disattiva variabile stamina
  end
})



local multiplier = 2.5
local shield = {
  initial_properties = {
    physical = false,
    collide_with_objects = false,
    pointable = false,
    visual = "upright_sprite",
    textures = {"bl_skill_shield_texture.png", "bl_skill_shield_texture.png"},
    use_texture_alpha = true,
    visual_size = {x = 1 * multiplier, y = 1 * multiplier, z = 1 * multiplier},
    collisionbox = {0,0,0,0,0,0},
  },

  --[[on_death = function(self, killer)
    local p_name = killer:get_player_name()
    if not arena_lib.is_player_in_arena(p_name) then return end
  end,]]
}

core.register_entity("block_league:shield", shield)