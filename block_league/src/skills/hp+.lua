local S = core.get_translator("block_league")

-- there is no "+" in the registration name as it causes issues when retrieving images names
skills.register_skill("block_league:hp", {
  name = "HP+",
  icon = "bl_skill_hp.png",
  _profile_description = S("Increases your health points by 25 @1(100>125)", "<style color=#abc0c0>") .. "</style>\n\n"
    .. S("Great for remaining in action longer, providing firepower to sustain your team. Get tanky!"),
  passive = true,
  on_start = function(self)
    local player = self.player
    player:set_properties({hp_max = 125})
    player:set_hp(125)
  end,
  on_stop = function(self)
    self.player:set_properties({hp_max = core.PLAYER_MAX_HP_DEFAULT})
  end
})
