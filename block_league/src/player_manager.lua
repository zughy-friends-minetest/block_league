local S = core.get_translator("block_league")



core.register_on_joinplayer(function(player)
  player:get_meta():set_string("bl_profile_elem_active", "")

  local p_name = player:get_player_name()

  block_league.init_equip(p_name)
  block_league.load_player_data(p_name)
  block_league.HUD_medals_create(p_name)

  -- se il server è crashato, disabilito l'abilità rimasta
  p_name:disable_skill(block_league.get_equipped_skill(p_name))
end)
