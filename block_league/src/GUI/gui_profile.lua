local S = core.get_translator("block_league")

local function get_formspec() end
local function get_weapon_details() end
local function get_skins() end
local function calc_button() end
local function get_variant() end
local function equip() end


-- `curr_elem` è sempre l'elemento originale, le varianti son salvate in `curr_var`.
-- `curr_elem` è spogliato da `block_league:`, `curr_var` no.
-- `curr_var` può equivalere anche alla base se non c'è una variante selezionata.
-- `curr_equipped` serve per non chiamare ogni volta la funzione per ottenere le armi
-- e viene aggiornata quando l'equipaggiamento cambia
local p_equipping = {}      -- KEY: p_name; VALUE: {is_equipping = true/nil, (string) section, (string) curr_elem, (string) curr_var, (table) curr_equipped}



function block_league.show_profile(p_name)
  p_equipping[p_name] = {curr_elem = "", curr_equipped = table.key_value_swap(block_league.get_equipped_weapons(p_name)), section = ""}
  core.show_formspec(p_name, "block_league:profile", get_formspec(p_name))
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------
---
function get_formspec(p_name)
  local p_props   = core.get_player_by_name(p_name):get_properties()
  local p_weaps   = block_league.get_equipped_weapons(p_name)
  local p_skill   = block_league.get_equipped_skill(p_name)
  local skill_def = skills.get_skill_def(p_skill)
  local info_section = {}
  local p_data = p_equipping[p_name]
  local elem = p_data.curr_elem
  local bl_curr_elem = "block_league:" .. elem

  -- calcolo contenuto da metter sulla destra
  if elem == "" then
    info_section = { "hypertext[0.3,1;4.48,7;elem_desc;<global size=15 halign=center valign=middle><i>" .. S("Welcome to your Block League profile!") .. "\n"
      .. S("Here you can change your equipment and learn about weapons and skills: select one from the panel on the left to know more about it") .. "\n\n"
      .. S("More customisations will be possible in the future@1 (donations help)", "<action name=donate url=https://liberapay.com/Zughy><style color=#7a9090>") .. "</style></i>]"
    }

  else
    local upper, switch, body, button
    local weap = core.registered_items[p_data.curr_var]
    local skill = skills.get_skill_def(bl_curr_elem)

    -- se è un'arma..
    if weap then
      local preview = weap.mesh and "model[0,1.5;5.08,2.2;weap_model;" .. weap.mesh .. ";" .. table.concat(weap.tiles, ",") .. ";0,140;false;true]"
                       or "image[2,1.7;1.5,1.5;" .. weap.wield_image .. "]"
      local var_name = weap._variant_name or ""

      upper = {
        "hypertext[0,0.35;5.08,0.8;elem_name;<global size=24 valign=middle halign=center><b>" .. weap.description .. "</b>]",
        "hypertext[0,0.7;5.08,0.9;elem_name;<global size=14 valign=middle halign=center><i><style color=#d5dfdf>" .. var_name .. "</i>]",
        preview,
        "hypertext[0.3,4.1;4.48,0.9;elem_desc;<global size=15 halign=center valign=middle><style color=#abc0c0><i>" .. weap._profile_description .. "</i>]"
      }

      if p_data.section ~= "skins" then
        switch = {
          "container[0.1,0]",
          "image[0.25,1.7;0.65,1.6;bl_gui_switch_bg.png]",
          "image[0.25,1.7;0.65,0.8;bl_gui_switch_sel.png^[colorize:#000000:20]",
          "image_button[0.28,1.8;0.57,0.57;bl_gui_info.png;info;;true;false;]",
          "image_button[0.28,2.6;0.57,0.57;bl_gui_skins.png^[multiply:#777777;skins;;true;false;]",
          "tooltip[info;" .. S("Info") .. "]",
          "tooltip[skins;" .. S("Skins") .. "]",
          "container_end[]",
        }
        body = get_weapon_details(weap)
      else
        switch = {
          "container[0.1,0]",
          "image[0.25,1.7;0.65,1.6;bl_gui_switch_bg.png]",
          "image[0.25,2.5;0.65,0.8;bl_gui_switch_sel.png^[colorize:#000000:20]",
          "image_button[0.28,1.8;0.57,0.57;bl_gui_info.png^[multiply:#777777;info;;true;false;]",
          "image_button[0.28,2.6;0.57,0.57;bl_gui_skins.png;skins;;true;false;]",
          "container_end[]",
        }
        body = get_skins(p_name, weap)
      end

      button = calc_button(p_name, p_data, bl_curr_elem)

      -- togliere quando ci sarà sistema d'exp
      if not core.check_player_privs(p_name, "blockleague_admin") then
        switch = {}
      end

    -- se è un'abilità..
    elseif skill then
      upper = {
        "hypertext[0,0.35;5.08,0.8;elem_name;<global size=24 valign=middle halign=center><b>" .. skill.name .. "</b>]",
        "image[2,1.7;1.5,1.5;bl_skill_" .. elem .. ".png]",
        "hypertext[0.3,4.1;4.48,4.3;elem_desc;<global size=15 halign=center><style color=#abc0c0><i>" .. skill._profile_description .. "</i>]"
      }
      switch = {}
      body = ""

      if bl_curr_elem ~= p_skill then
        button = "image_button[1.45,7.9;2.2,0.8;bl_gui_profile_button_equip.png;equip;" .. S("EQUIP") .. "]"
      end
    end

    upper = table.concat(upper, "")
    switch = table.concat(switch,"")

    info_section = {
      upper,
      switch,
      "container[0.4,5.2]",
      body,
      "container_end[]",
      button
    }
  end

  local right_elem = table.concat(info_section, "")
  local curr_achvmt_amnt = #achvmt_lib.get_player_achievements(p_name, {mod = "block_league"})
  local all_achvmt_amnt = achvmt_lib.get_amount({mod = "block_league"})

  -- corpo
  local formspec = {
    "formspec_version[4]",
    "size[19,9,true]",
    "no_prepend[]",
    "background[0,0;19,9;bl_gui_profile_bg.png]",
    "bgcolor[;true]",
    "style_type[item_image_button;border=false]",
    "style_type[image_button;border=false]",
    "style[equip,change,equip_locked,change_locked;font=bold]",
    "style[equip_locked,change_locked;textcolor=#666666]",
    "style[equip:hovered;fgimg=bl_gui_profile_button_equip_on.png]",
    "style[change:hovered;fgimg=bl_gui_profile_button_change_on.png]",
    "style[weap,wslot,sslot;font=mono;textcolor=#00000000]",
    "listcolors[#ffffff;#ffffff;#ffffff;#3153b7;#ffffff]",

    -- parte sinistra
    "model[0.08,0.8;5.08,3.6;chara;" .. p_props.mesh .. ";" .. table.concat(p_props.textures, ",") .. ";0,-150;false;true]",
    "container[0.49,5]",
    "image[0,0;1.05,1.05;bl_gui_profile_button_weap.png]]",
    "image[1.1,0;1.05,1.05;bl_gui_profile_button_weap.png]",
    "image[2.2,0;1.05,1.05;bl_gui_profile_button_weap.png]",
    "image[3.3,0;1.05,1.05;bl_gui_profile_button_skill.png]",
    "image_button[0.1,0.11;0.85,0.85;" .. (p_weaps[1] and core.registered_nodes[p_weaps[1]].inventory_image or "blank.png") .. ";weap;1]",
    "image_button[1.2,0.11;0.85,0.85;" .. (p_weaps[2] and core.registered_nodes[p_weaps[2]].inventory_image or "blank.png") .. ";weap;2]",
    "image_button[2.3,0.11;0.85,0.85;" .. (p_weaps[3] and core.registered_nodes[p_weaps[3]].inventory_image or "blank.png") .. ";weap;3]",
    "image_button[3.4,0.11;0.85,0.85;" .. skill_def.icon .. ";skill;]",
    "tooltip[skill;" .. skill_def.name .. "]",
    "container[0.05,1.55]",
    "image[0,0;0.8,0.8;bl_rank_beginner.png]",
    "hypertext[0.9,0;3.25,0.8;pname_txt;<global size=24 valign=middle><b>" .. p_name .. "</b>]",
    "image[0.05,1;0.22,0.22;bl_gui_profile_infobox_trophies.png]",
    "image[0.05,1.8;0.22,0.3;bl_gui_profile_infobox_money.png]",
    "hypertext[0.4,0.94;3.35,0.4;pname_txt;<global size=14 valign=middle><b>" .. curr_achvmt_amnt .. "/" .. all_achvmt_amnt .. "</b>]",
    "hypertext[0.4,1.78;3.35,0.4;pname_txt;<global size=14 valign=middle><b>---</b>]",
    "container_end[]",
    "container_end[]",

    -- parte centrale
    "container[5.85,0.35]",
    "hypertext[0,0;3.35,0.9;weap_txt;<global size=24 valign=middle><style color=#5be7b1><b>" .. S("Weapons") .. "</b>]",
    "image[0,1;1.05,1.05;bl_gui_profile_inv_weapon_unlocked.png]",
    "image[1.25,1;1.05,1.05;bl_gui_profile_inv_weapon_unlocked.png]",
    "image[2.5,1;1.05,1.05;bl_gui_profile_inv_weapon_unlocked.png]",
    "image[3.75,1;1.05,1.05;bl_gui_profile_inv_weapon_unlocked.png]",
    "image[5,1;1.05,1.05;bl_gui_profile_inv_weapon_unlocked.png]",
    "image[6.25,1;1.05,1.05;bl_gui_profile_inv_weapon_unlocked.png]",
    "image_button[0.1,1.11;0.85,0.85;bl_smg.png;wslot;smg]",
    "image_button[1.35,1.11;0.85,0.85;bl_sword.png;wslot;sword]",
    "image_button[2.6,1.11;0.85,0.85;bl_pixelgun.png;wslot;pixelgun]",
    "image_button[3.85,1.11;0.85,0.85;bl_peacifier.png;wslot;peacifier]",
    "image_button[5.10,1.11;0.85,0.85;bl_sentrygun.png;wslot;sentry_gun]",
    "image_button[6.35.10,1.11;0.85,0.85;bl_bomb.png;wslot;bomb]",
    "container[0,4.5]",
    "hypertext[0,0;3.35,0.9;weap_txt;<global size=24 valign=middle><style color=#5be7b1><b>" .. S("Skills") .. "</b>]",
    "image[0,1;1.05,1.05;bl_gui_profile_inv_skill_unlocked.png]",
    "image[1.25,1;1.05,1.05;bl_gui_profile_inv_skill_unlocked.png]",
    "image[2.5,1;1.05,1.05;bl_gui_profile_inv_skill_locked.png]",
    "image_button[0.1,1.11;0.85,0.85;bl_skill_hp.png;sslot;hp]",
    "image_button[1.35,1.11;0.85,0.85;bl_skill_sp.png;sslot;sp]",
    "image[2.6,1.11;0.85,0.85;bl_skill_shield.png^[multiply:#777777]",
    "hypertext[2.53,1.05;0.95,0.95;soon_tm;<global size=13 halign=center valign=middle><style color=#abc0c0><b>" .. S("Soon") .. "</b>]",
    "container_end[]",
    "container_end[]",

    -- parte destra
    "container[13.88,0]",
    "image_button[5.2,0;0.8,0.8;bl_gui_profile_close.png;close;;true;false;]",
    right_elem,
    "container_end[]"
  }

  return table.concat(formspec, "")
end



function get_weapon_details(weap)
  local action_y = 0
  local action1, action1_hold, action1_air, action2, action2_hold, action2_air

  -- azioni varie
  if weap.action1 then
    action1 = "image[0," .. action_y .. ";0.4,0.55;bl_gui_profile_action_lmb.png]" ..
              "hypertext[0.6," .. action_y - 0.12 .. ";3.8,0.8;elem_desc;<global size=15 valign=middle><i>" .. weap.action1._description .. "</i>]"
    action_y = action_y + 0.8
  end

  if weap.action1_hold then
    action1_hold = "image[0," .. action_y .. ";0.4,0.55;bl_gui_profile_action_lmb_hold.png]" ..
              "hypertext[0.6," .. action_y - 0.12 .. ";3.8,0.8;elem_desc;<global size=15 valign=middle><i>" .. weap.action1_hold._description .. "</i>]"
    action_y = action_y + 0.8
  end

  if weap.action1_air then
    action1_air = "image[0," .. action_y .. ";0.4,0.55;bl_gui_profile_action_lmb_air.png]" ..
              "hypertext[0.6," .. action_y - 0.12 .. ";3.8,0.8;elem_desc;<global size=15 valign=middle><i>" .. weap.action1_air._description .. "</i>]"
    action_y = action_y + 0.8
  end

  if weap.action2 then
    action2 = "image[0," .. action_y .. ";0.4,0.55;bl_gui_profile_action_rmb.png]" ..
              "hypertext[0.6," .. action_y - 0.12 .. ";3.8,0.8;elem_desc;<global size=15 valign=middle><i>" .. weap.action2._description .. "</i>]"
    action_y = action_y + 0.8
  end

  if weap.action2_hold then
    action2_hold = "image[0," .. action_y .. ";0.4,0.55;bl_gui_profile_action_rmb_hold.png]" ..
              "hypertext[0.6," .. action_y - 0.12 .. ";3.8,0.8;elem_desc;<global size=15 valign=middle><i>" .. weap.action2_hold._description .. "</i>]"
    action_y = action_y + 0.8
  end

  if weap.action2_air then
    action1_air = "image[0," .. action_y .. ";0.4,0.55;bl_gui_profile_action_rmb_air.png]" ..
              "hypertext[0.6," .. action_y - 0.12 .. ";3.8,0.8;elem_desc;<global size=15 valign=middle><i>" .. weap.action2_air._description .. "</i>]"
    action_y = action_y + 0.8
  end

  local ammo = ""

  if weap.weapon_type ~= "melee" and weap.magazine then
    local max_ammo = weap.limited_magazine and 0 or "--"
    local reload_time = weap.reload_time == -1 and "---" or weap.reload_time

    ammo = table.concat({
      "image[0,1.6;0.4,0.4;bl_gui_profile_weapon_magazine.png]",
      "image[3,1.6;0.4,0.4;bl_gui_profile_weapon_reload.png]",
      "hypertext[0.6,1.53;1.1,0.6;elem_desc;<global size=16 valign=middle><i>" .. weap.magazine .. "  / " .. max_ammo .. "</i>]",
      "hypertext[3.6,1.53;1,0.6;elem_desc;<global size=16 valign=middle><i>" .. reload_time .. "</i>]"
    })
  end

  local attributes = table.concat({
    action1       or "",
    action1_hold  or "",
    action1_air   or "",
    action2       or "",
    action2_hold  or "",
    action2_air   or "",
    ammo
  }, "")

  return attributes
end



function get_skins(p_name, weap)
  if not weap._variants then return "" end

  -- se è una variante, prendi l'originale
  if weap._variant_of then
    weap = core.registered_items[weap._variant_of]
  end

  local w_name = weap.name
  local list = "image_button[0,0;4.3,0.7;bl_gui_profile_button_skin.png;skin;" .. S("Default") .. "]image[0.2,0.08;0.5,0.5;" .. weap.inventory_image .. "]"
  local y = 0.7

  for _, var_name in ipairs(weap._variants) do
    local variant = core.registered_items[var_name]
    if block_league.has_variant(p_name, w_name, var_name) then
      local curr_equipped = p_equipping[p_name].curr_equipped[var_name] and "^[hsl:0:15:20" or ""
      list = list .. "image_button[0," .. y .. ";4.3,0.7;bl_gui_profile_button_skin.png" .. curr_equipped .. ";skin;"
          .. variant._variant_name .. "]image[0.2," .. y + 0.08 .. ";0.5,0.5;" .. variant.inventory_image .. "]"
    else
      list = list .. "image_button[0," .. y .. ";4.3,0.7;bl_gui_profile_button_skin.png^[multiply:#777777;skin_locked;"
          .. variant._variant_name .. "]image[0.2," .. y + 0.08 .. ";0.5,0.5;" .. variant.inventory_image .. "^[multiply:#777777]"
    end
    y = y + 0.7
  end

  local var_amount = #weap._variants
  local scrollbar_max_val = var_amount < 3 and 0 or 2.8 + (var_amount - 3) * 7 -- ogni casella è alta 7, prob fa 0.7 * 10

  local fs = {
    "style[skin,skin_locked;font_size=15]",
    "style[skin:hovered;fgimg=bl_gui_profile_button_skin_on.png]",
    "style[skin_locked;textcolor=#666666]",
    "scroll_container[0,-0.2;4.3,2.6;skins_scroll;vertical]",
    list,
    "scroll_container_end[]",
    "scrollbaroptions[max=" .. scrollbar_max_val .. ";arrows=hide;thumbsize=3;smallstep=4]",
    "scrollbar[14.2,-0.29;0.3,2.6;vertical;skins_scroll;0]" -- messa fuori schermo perché è davvero brutta da vedere e non posso personalizzarla (https://github.com/luanti-org/luanti/issues/9692). Tanto funziona uguale
  }

  return table.concat(fs, "")
end



-- TODO da estendere ad abilità in futuro
function calc_button(p_name, p_data, bl_curr_elem)
  local button = ""

  if p_data.is_equipping then
    button =  "image[-13.88,0;19,9;bl_gui_profile_bg_mask.png^[opacity:160]" ..
              "hypertext[0.3,7.9;4.48,0.9;equip_desc;<global size=14 halign=center><style color=#abc0c0>" .. S("Choose the slot where to equip the weapon") .. "]"
  else
    if p_data.section ~= "skins" then
      if block_league.has_weapon(p_name, bl_curr_elem) then
        button = "image_button[1.45,7.9;2.2,0.8;bl_gui_profile_button_equip.png;equip;" .. S("EQUIP") .. "]"
      else
        button = "image_button[2.6,7.9;2.2,0.8;bl_gui_profile_button_equip.png^[multiply:#777777;equip_locked;" .. S("EQUIP") .. "]"
      end

    else
      if block_league.has_weapon(p_name, bl_curr_elem) then
        local weap_orig = core.registered_items[bl_curr_elem]
        local var_name = p_data.curr_var
        if (weap_orig._variants and #weap_orig._variants > 0)
           and not p_data.curr_equipped[var_name]
           and block_league.has_weapon_equipped(p_name, weap_orig.name, true)
           and block_league.has_variant(p_name, weap_orig.name, var_name) then
          button = "image_button[0.3,7.9;2.2,0.8;bl_gui_profile_button_change.png;change;" .. S("CHANGE") .. "]"
        else
          button = "image_button[0.3,7.9;2.2,0.8;bl_gui_profile_button_change.png^[multiply:#777777;change_locked;" .. S("CHANGE") .. "]"
        end

        if block_league.has_variant(p_name, weap_orig.name, var_name) then
          button = button .. "image_button[2.6,7.9;2.2,0.8;bl_gui_profile_button_equip.png;equip;" .. S("EQUIP") .. "]"
        else
          button = button .. "image_button[2.6,7.9;2.2,0.8;bl_gui_profile_button_equip.png^[multiply:#777777;equip_locked;" .. S("EQUIP") .. "]"
        end
      else
        button = "image_button[0.3,7.9;2.2,0.8;bl_gui_profile_button_change.png^[multiply:#777777;change_locked;" .. S("CHANGE") .. "]"
        .. "image_button[2.6,7.9;2.2,0.8;bl_gui_profile_button_equip.png^[multiply:#777777;equip_locked;" .. S("EQUIP") .. "]"
      end
    end
  end

  -- TODO per quando si potranno rimuovere armi
  --[[if block_league.has_weapon_equipped(p_name, bl_curr_elem) then
    button = "image_button[1.45,7.9;2.2,0.8;bl_gui_profile_button_unequip.png;unequip;" .. S("UNEQUIP") .. "]"
  else
    button = "image_button[1.45,7.9;2.2,0.8;bl_gui_profile_button_equip.png;equip;" .. S("EQUIP") .. "]"
  end]]

  return button
end



function get_variant(p_data, bl_curr_elem)
  for ww_name, _ in pairs(p_data.curr_equipped) do
    local wweap = core.registered_items[ww_name]

    if wweap._variant_of and wweap._variant_of == bl_curr_elem then
      return ww_name
    end
  end

  return bl_curr_elem
end



function equip(type, p_name, p_data, elem_technical, slot)
  if type == "weapon" then
    block_league.equip(p_name, elem_technical, slot)
    p_data.curr_equipped = table.key_value_swap(block_league.get_equipped_weapons(p_name))
  else
    block_league.set_equipped_skill(p_name, elem_technical)
  end

  audio_lib.play_sound("bl_gui_equip_confirm", {to_player = p_name})
end





----------------------------------------------
---------------GESTIONE CAMPI-----------------
----------------------------------------------

core.register_on_player_receive_fields(function(player, formname, fields)
  if formname ~= "block_league:profile" then return end

  local p_name = player:get_player_name()
  local p_data = p_equipping[p_name]
  local is_equipping = p_data.is_equipping

  if fields.quit and not is_equipping then return end -- non chiudere se sto equipaggiando e premo Esc

  local bl_curr_elem = "block_league:" .. p_data.curr_elem
  local elem_technical = p_data.curr_var or bl_curr_elem -- al contrario di `curr_elem`, questo prende eventuale variante. TODO: vedere quando inserite abilità se ha senso

  if fields.close then
    core.close_formspec(p_name, "block_league:profile")
    p_data = nil
    return
  end

  -- sezione (non cambiare se clicco sulla casella di un'arma)
  if fields.info or fields.sslot then
    p_data.section = "info"
  elseif fields.skins then
    p_data.section = "skins"
  end

  if is_equipping and not fields.weap then
    p_data.is_equipping = nil
  end

  if fields.weap then
    local slot = tonumber(fields.weap)

    if not type(slot) == "number" or slot < 1 or slot > 3 then return end -- sanity check

    if is_equipping then
      if not weapons_lib.is_weapon(elem_technical) or not block_league.has_weapon(p_name, bl_curr_elem) then return end

      equip("weapon", p_name, p_data, elem_technical, slot)
      p_data.is_equipping = nil

    else
      local p_w_name = block_league.get_equipped_weapons(p_name)[slot]

      if not p_w_name then
        p_data.curr_elem = ""
      else
        local p_weap = core.registered_items[p_w_name]

        -- prendi l'originale x curr_elem
        if p_weap._variant_of then
          p_w_name = p_weap._variant_of
        end

        local curr_elem = string.sub(p_w_name, 14, -1)

        p_data.curr_elem = curr_elem
        p_data.curr_var = get_variant(p_data, "block_league:" .. curr_elem)
      end
    end

  elseif fields.skill then
    p_data.curr_elem = string.sub(block_league.get_equipped_skill(p_name), 14, -1)
  elseif fields.wslot then
    p_data.curr_elem = fields.wslot
    p_data.curr_var = get_variant(p_data, "block_league:" .. fields.wslot)
  elseif fields.sslot then
    p_data.curr_elem = fields.sslot

  elseif fields.skin or fields.skin_locked then
    local skin_name = fields.skin or fields.skin_locked
    if skin_name == S("Default") then
      p_data.curr_var = bl_curr_elem
    else
      local curr_weap = core.registered_items[bl_curr_elem]
      for _, var_name in ipairs(curr_weap._variants) do
        if core.registered_items[var_name]._variant_name == skin_name then
          p_data.curr_var = var_name
        end
      end
    end

    core.show_formspec(p_name, "block_league:profile", get_formspec(p_name))

  elseif fields.change then
    local weap = core.registered_items[elem_technical]

    if weap then
      -- validazione
      if not weapons_lib.is_weapon(elem_technical) or
         not block_league.has_weapon_equipped(p_name, weap._variant_of or bl_curr_elem, true) or
         not block_league.has_variant(p_name, weap._variant_of or bl_curr_elem, elem_technical) then
        return end

      for slot, ww_name in ipairs(block_league.get_equipped_weapons(p_name)) do
        ww_name = core.registered_items[ww_name]._variant_of or ww_name
        if ww_name == bl_curr_elem then
          equip("weapon", p_name, p_data, elem_technical, slot)
        end
      end
    else
      -- TODO: abilità
    end

  elseif fields.equip then
    local weap = core.registered_items[elem_technical]

    if weap then
      if block_league.has_weapon(p_name, weap._variant_of or bl_curr_elem) then
        p_data.is_equipping = true
      end

    else
      equip("skill", p_name, p_data, elem_technical)
    end
  end

  core.show_formspec(p_name, "block_league:profile", get_formspec(p_name))
end)