local S = core.get_translator("block_league")

local mod = "block_league"

local cmd_bleague = chatcmdbuilder.register("bleague", {})
local cmd_bladmin = chatcmdbuilder.register("bladmin", {
  description = S("mod management"),
  privs = { blockleague_admin = true }
})

--------------------------------bladmin-------------------------------------------

-- rinominazione arene
cmd_bladmin:sub("rename :arena :newname", function(sender, arena_name, new_name)
    arena_lib.rename_arena(sender, mod, arena_name, new_name)
end)

cmd_bladmin:sub("testkit", function(sender)
  block_league.enter_test_mode(sender)
end)

-- need it to allow command blocks to run /bladmin @nearest, or I don't know how to open players' profiles through pressure plates and the like
cmd_bladmin:sub("profile :playername", function(sender, p_name)
  if arena_lib.is_player_in_arena(sender) or not core.get_player_by_name(p_name) then
    core.chat_send_player(sender, core.colorize("#e6482e", S("[!] You can't perform this action right now!")))
    return end

  block_league.show_profile(p_name)
end)

--------------- equipaggiamento: armi
--[[cmd_bladmin:sub("unlock :playername weapon :wname", function(sender, p_name, w_name)
  local success, output = block_league.unlock_weapon(p_name, w_name)

  if success then
    block_league.print(sender, output)
  else
    block_league.print_error(sender, output)
  end
end)]]

cmd_bladmin:sub("unlockvar :playername weapon :wname :varname", function(sender, p_name, w_name, var_name)
  local success, output = block_league.unlock_weapon_variant(p_name, w_name, var_name)

  if success then
    block_league.print(sender, output)
  else
    block_league.print_error(sender, output)
  end
end)

--[[cmd_bladmin:sub("remove :playername weapon :wname", function(sender, p_name, w_name)
  local success, output = block_league.remove_weapon(p_name, w_name)

  if success then
    block_league.print(sender, output)
  else
    block_league.print_error(sender, output)
  end
end)]]

cmd_bladmin:sub("removevar :playername weapon :wname :varname", function(sender, p_name, w_name, var_name)
  local success, output = block_league.remove_weapon_variant(p_name, w_name, var_name)

  if success then
    block_league.print(sender, output)
  else
    block_league.print_error(sender, output)
  end
end)

--------------- equipaggiamento: abilità TODO
cmd_bladmin:sub("unlock :playername skill :sname", function(sender, p_name, c_name)
end)

cmd_bladmin:sub("unlockvar :playername skill :sname :varname", function(sender, p_name, s_name, var_name)
end)

cmd_bladmin:sub("remove :playername skill :sname", function(sender, p_name, c_name)
end)

cmd_bladmin:sub("removevar :playername skill :sname", function(sender, p_name, c_name)
end)





--------------------------------bleague-------------------------------------------

cmd_bleague:sub("profile", function(sender)
  if arena_lib.is_player_in_arena(sender) then
    core.chat_send_player(sender, core.colorize("#e6482e", S("[!] You can't perform this action right now!")))
    return end

  block_league.show_profile(sender)
end)
