local S = core.get_translator("block_league")

local achievements = {
  ["block_league:two_in_one"]   = { title = S("Two in One"), img = "bl_achievement_doublekill.png", desc = S("Kill 2 players with 1 shot of pixelgun"), tier = "Silver"},
  ["block_league:three_in_one"] = { title = S("Three in One"), img = "bl_achievement_triplekill.png", desc = S("Kill 3 players with 1 shot of pixelgun"), tier = "Gold"  },
  -- TODO: bomba: uccidi unə suicidandoti, uccidine 3 in un colpo
}

achvmt_lib.register_mod("block_league", {
  name = S("Block League"),
  icon = "bl_pixelgun.png"
})

--[[for name, ach in pairs(achievements) do
  achvmt_lib.register_achievement(name,{
    title = ach.title,
    description = ach.desc,
    image = ach.img,
    tier = ach.tier,
    hidden = false,
  })
end]]
