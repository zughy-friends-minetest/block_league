local S = core.get_translator("block_league")

local dmg1      = 15
local dmg1hold  = 34
local dmg1air   = 18.5
local dmg2      = 35

local function register_sword(name, def)
  if def.variant_of then
    table.insert(core.registered_items[def.variant_of]._variants, name)
  end

  weapons_lib.register_weapon(name, {
    description = S("2H Sword"),
    _profile_description = S("Keep your friends close and your enemies further -Sun Tzu"),
    _variant_name = def.variant_name,
    _variant_of = def.variant_of,
    _variants = {},
    groups = {bl_sword = 1},

    wield_image = def.icon,
    wield_scale = {x=1.3, y=1.3, z=1.3},
    inventory_image = def.icon,

    weapon_type = "melee",
    slow_down_user = true,

    crosshair = "bl_sword_crosshair.png",

    can_use_weapon = function(player, action)
      return block_league.util_weapon_use_check(player)
    end,

    can_alter_speed = function(player)
      return block_league.util_on_can_alter_speed(player:get_player_name())
    end,

    on_after_hit = function(hitter, weapon, action, objects_hit, total_damage)
      block_league.util_on_after_hit(hitter, weapon, objects_hit, total_damage)
    end,

    on_recovery_end = function(player, weapon, action)
      block_league.util_on_recovery_end(player, weapon, action)
    end,

    --[[action1 = {
      type = "punch",
      description = S("slash, @1♥", "<style color=#f66c77>" .. dmg1),
      damage = dmg1,
      delay = 0.4,
      sound = "bl_sword_hit",
    },]]

    -- TEMP: questa dovrebbe diventare action1_hold una volta che:
    --   1. Sarà possibile personalizzare l'animazione dell'oggetto tenuto in mano.
    --   Vedasi https://github.com/minetest/minetest/issues/2811.
    --   2. Sarà possibile ritardare l'azione del clic.
    --   Vedasi https://github.com/minetest/minetest/issues/13733
    action1 = {
      type = "punch",
      _description = S("push, @1♥", "<style color=#f66c77>" .. dmg1hold),
      damage = dmg1hold,
      knockback = 40,
      delay = 1.2,
      sound = "bl_sword_hit",

      on_hit = function(hitter, target, weapon, action, hit_point, damage, knockback)
        return block_league.util_on_hit(hitter, target, weapon, action, hit_point, damage, knockback)
      end,

      on_end = function(player, weapon, action)
        block_league.util_on_end_melee(player:get_player_name(), weapon.name)
      end,
    },

    --[[action1_air = {
      type = "custom",
      description = S("Dive onto the ground and stun enemies in front of you, @1♥", "<style color=#f66c77>" .. dmg1air),
      damage = dmg1air,
      -- loading_time = 0.3,
      delay = 1, -- poi abbassa a 0.7
      physics_override = "FREEZE",
      sound = "bl_sword_dash",

      on_use = function(player, weapon, action)
        local dummy = player:get_attach()
        dummy:set_velocity({x = 0, y = -16, z = 0})

        core.after(0.5, function()
          local ent_pos = dummy:get_pos()
          core.add_particlespawner({
            amount = 50,
            time = 0.6,
            minpos = ent_pos,
            maxpos = ent_pos,
            minvel = {x=-2, y=-2, z=-2},
            maxvel = {x=2, y=2, z=2},
            minsize = 1,
            maxsize = 3,
            texture = "arenalib_winparticle.png"
          })

          -- TODO: metti particellare appropriato; dannegga chi è in zona, 30° x lato

          core.after(0.5, function()
            dummy:remove()
          end)
        end)
      end
    },]]

    action2 = {
      type = "custom",
      _description = S("dash forward, @1♥", "<style color=#f66c77>" .. dmg2),
      damage = dmg2,
      delay = 2.5,
      physics_override = { speed = 0.5, jump = 0 },
      sound = "bl_sword_dash",

      on_use = function(player, weapon, action)
        local pointed_objects = weapons_lib.get_pointed_objects(player, 5, true)
        local dir = player:get_look_dir()

        dir.y = 0

        local player_vel = player:get_velocity()
        local sprint = vector.multiply(dir,18)

        player:add_velocity(sprint)
        player_vel = vector.multiply(player_vel, -0.7)
        player_vel.y = 0 -- sennò può schizzare in aria cadendo da una certa altezza e mirando verso l'alto (clic quando atterra)
        player:add_velocity(player_vel)

        if not pointed_objects then return end
        weapons_lib.apply_damage(player, pointed_objects, weapon, action)
      end,

      on_hit = function(hitter, target, weapon, action, hit_point, damage, knockback)
        return block_league.util_on_hit(hitter, target, weapon, action, hit_point, damage, knockback)
      end,
    }
  })
end



register_sword("block_league:sword", {
  icon = "bl_sword.png",
})

register_sword ("block_league:sword_silver", {
  icon = "bl_sword_silver.png",
  variant_name = S("Silver"),
  variant_of = "block_league:sword"
})

register_sword("block_league:sword_gold", {
  icon = "bl_sword_gold.png",
  variant_name = S("Gold"),
  variant_of = "block_league:sword"
})

register_sword("block_league:sword_diamond", {
  icon = "bl_sword_diamond.png",
  variant_name = S("Diamond"),
  variant_of = "block_league:sword"
})