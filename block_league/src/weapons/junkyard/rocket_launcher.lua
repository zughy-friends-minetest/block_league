-- breaks the gameplay too much

local S = core.get_translator("block_league")
local dmg = 8

audio_lib.register_sound("sfx", "bl_rocketlauncher_shoot", S("Rocket Launcher shoots"))

weapons_lib.register_weapon("block_league:rocket_launcher", {
  description = S("Rocket Launcher"),
  _profile_description = "Is it a bird? No, it's a rocket!",
  groups = {bl_weapon_mesh = 1},

  mesh = "bl_rocketlauncher.obj",
  tiles = {"bl_rocketlauncher_texture.png"},
  wield_scale = {x=1.3, y=1.3, z=1.3},
  inventory_image = "bl_rocketlauncher.png",
  crosshair = "bl_smg_crosshair.png",

  weapon_type = "gun",
  magazine = 1,
  reload_time = 1.6,

  can_use_weapon = function(player, action)
    return block_league.util_weapon_use_check(player)
  end,

  can_alter_speed = function(player)
    return block_league.util_on_can_alter_speed(player:get_player_name())
  end,

  on_reload = function(player, weapon)
    block_league.util_on_reload(player:get_player_name(), weapon.name)
  end,

  on_reload_end = function(player, weapon)
    block_league.util_on_reload_end(player:get_player_name(), weapon.name)
  end,

  on_after_hit = function(hitter, weapon, action, objects_hit, total_damage)
    block_league.util_on_after_hit(hitter, weapon, objects_hit, total_damage)
  end,

  action1 = {
    type = "bullet",
    _description = S("Shoots a rocket, area damage, can hurt yourself, @1♥", "<style color=#f66c77>" .. dmg),
    damage = dmg,
    delay = 0.8,

    sound = "bl_rocketlauncher_shoot",

    on_use = function(player, weapon, action)
      block_league.util_update_mag_hud(player:get_player_name(), weapon.name)
    end,

    on_hit = function(hitter, target, weapon, action, hit_point, damage)
      return block_league.util_on_hit(hitter, target, weapon, action, hit_point, damage)
    end,

    bullet = {
      name = "block_league:rocket",

      mesh = "bl_rocket.obj",
      textures = {"bl_bullet_rocket.png"},
      visual_size = {x=1, y=1, z=1},
      collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},

      speed = 30,
      lifetime = 5,

      explosion = {
        range = 3.5,
        damage = 8,
        texture = "bl_rocket_particle.png",
      },

      remove_on_contact = true,
      gravity = false,

      trail = {
        image = "bl_bullet_rocket.png",
        life = 1,
        size = 2,
        interval = 5, -- in step
        amount = 20,
      }
    }
  },

})
