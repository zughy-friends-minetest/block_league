local S = core.get_translator("block_league")
local dmg = 110

local function ignite_sound_and_animation() end

local bomb_bullet = {
  initial_properties = {
    name = "block_league:bomb",
    visual = "mesh",
    mesh = "bl_bomb.obj",
    textures = {"bl_bomb_texture.png"},
    collisionbox = {-0.35, -0.35, -0.35, 0.35, 0.35, 0.35},
    visual_size = {x=5, y=5},

    physical = true,
    collide_with_objects = false
  },

  _collided = false,
  _ignited = false,
  _teamID = nil,

  -- cambia texture a seconda del colore della squadra
  on_activate = function(self, staticdata)
    local p_name = staticdata
    local skin_txtr = core.get_player_by_name(p_name):get_wielded_item():get_definition().tiles
    local bomb = self.object

    bomb:set_properties({textures = skin_txtr})

    if not arena_lib.is_player_playing(self._p_name) then return end

    self._teamID = arena_lib.get_teamID(self._p_name)

    local col = self._teamID == 1 and "orange" or "blue"

    bomb:set_properties({textures = {bomb:get_properties().textures[1] .. "^[colorize:" .. col .. ":85"}})
  end,


  on_step = function(self, dtime, moveresult)
    local owner = core.get_player_by_name(self._p_name)

    -- distruggi se proprietariə muore
    if owner:get_hp() <= 0 then
      audio_lib.stop_sound(self._p_name, "bl_bomb_ignite")
      for _, obj in ipairs(core.get_objects_inside_radius(self.object:get_pos(), 20)) do
        if obj:is_player() then
          audio_lib.stop_sound(obj:get_player_name(), "bl_bomb_ignite")
        end
      end

      self:_destroy()
      return
    end

    if moveresult.collides then
      if not self._collided then
        self._collided = true
        ignite_sound_and_animation(self)
      end
    end

    if self._collided and not self._ignited then
      core.after(2, function()
        if not self then return end -- in caso venga fatta esplodere da altra bomba
        self:_destroy()
      end)
      self._ignited = true
    end
  end
}

-- esiste probabilmente una soluzione più pulita ma sono stanco
local bomb_bullet_silver = table.copy(bomb_bullet)
local bomb_bullet_gold = table.copy(bomb_bullet)
local bomb_bullet_diamond = table.copy(bomb_bullet)

bomb_bullet_silver.initial_properties.name = "block_league:bomb_silver"
bomb_bullet_gold.initial_properties.name = "block_league:bomb_gold"
bomb_bullet_diamond.initial_properties.name = "block_league:bomb_diamond"



local function register_bomb(name, def)
  if def.variant_of then
    table.insert(core.registered_items[def.variant_of]._variants, name)
  end

  weapons_lib.register_weapon(name, {
    description = S("Bomb"),
    _profile_description = S("K A B O O M !"),
    _variant_name = def.variant_name,
    _variant_of = def.variant_of,
    _variants = {},

    mesh = "bl_bomb.obj",
    tiles = def.textures,
    wield_scale = {x=0.8, y=0.8, z=0.8},
    inventory_image = def.icon,

    weapon_type = "ranged",
    magazine = 1,
    limited_magazine = true, -- TODO: sarebbe carino se potessi averne due in tutto, con capienza max 1
    reload_time = -1,

    crosshair = "bl_bomb_crosshair.png",

    can_use_weapon = function(player, action)
      return block_league.util_weapon_use_check(player)
    end,

    can_alter_speed = function(player)
      return block_league.util_on_can_alter_speed(player:get_player_name())
    end,

    on_after_hit = function(hitter, weapon, action, objects_hit, total_damage)
      block_league.util_on_after_hit(hitter, weapon, objects_hit, total_damage)
    end,

    action1 = {
      type = "bullet",
      _description = S("Throw, AoE damage, friendly fire. It dies with you, @1♥", "<style color=#f66c77>" .. dmg),
      damage = dmg,
      delay = 1,
      _friendly_fire = true,
      _friendly_dmg_perc = 0.25,

      sound = "bl_bomb_throw",

      trail = {
        image = "weaponslib_test1_trail2.png",
        life = 1,
        size = 2,
        amount = 5,
      },

      bullet = {
        entity = def.bullet,

        speed = 25,
        bounciness = 0,
        weight = 50,
        friction = 0.1,
        lifetime = 20,

        explosion = {
          range = 6,
          minimum_damage_perc = 0.3,
          texture = "weaponslib_test1_trail2.png",
          sound = "bl_bomb_explosion",
        },

        remove_on_contact = false,
        has_gravity = true,
      },

      on_use = function(player, weapon, action)
        block_league.util_update_mag_hud(player:get_player_name(), weapon.name)
      end,

      on_hit = function(hitter, target, weapon, action, hit_point, damage, knockback, proxy_obj)
        return block_league.util_on_hit(hitter, target, weapon, action, hit_point, damage, knockback, proxy_obj)
      end,
    }
  })
end



register_bomb("block_league:bomb", {
  icon = "bl_bomb.png",
  textures = {"bl_bomb_texture.png"},
  bullet = bomb_bullet
})

register_bomb("block_league:bomb_silver", {
  icon = "bl_bomb_silver.png",
  textures = {"bl_bomb_texture_silver.png"},
  bullet = bomb_bullet_silver,
  variant_name = S("Silver"),
  variant_of = "block_league:bomb"
})

register_bomb("block_league:bomb_gold", {
  icon = "bl_bomb_gold.png",
  textures = {"bl_bomb_texture_gold.png"},
  bullet = bomb_bullet_gold,
  variant_name = S("Gold"),
  variant_of = "block_league:bomb"
})

register_bomb("block_league:bomb_diamond", {
  icon = "bl_bomb_diamond.png",
  textures = {"bl_bomb_texture_diamond.png"},
  bullet = bomb_bullet_diamond,
  variant_name = S("Diamond"),
  variant_of = "block_league:bomb"
})





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function ignite_sound_and_animation(self)
  local bomb = self.object

  core.add_particlespawner({
    amount = 40,
    time = 2,
    attached = bomb,
    minvel = {x = -0.5, y =  0.1, z = -0.5},
    maxvel = {x = 0.5, y = 1, z = 0.5},
    minsize = 5,
    maxsize = 8,
    texture = {
        name = "bl_bomb_smoke.png",
        alpha_tween = {1, 0},
        scale_tween = {
          {x = 0.2, y = 0.2},
          {x = 1, y = 1},
      }
    },
  })

  weapons_lib.play_sound_area("bl_bomb_ignite", bomb:get_pos(), 8, self._p_name)

  local col = ""

  if self._teamID then
    col = "^[colorize:" .. (self._teamID == 1 and "orange" or "blue") .. ":85"
  end

  local orig_txtr = string.match(bomb:get_properties().textures[1], "^(.*)%.png")

  bomb:set_properties({textures = {orig_txtr .. "_igniting1.png" .. col}})

  core.after(1, function()
    if not self then return end
    bomb:set_properties({textures = {orig_txtr .. "_igniting1.png^[colorize:red:50" .. col}})

    core.after(0.8, function()
      if not self then return end
      bomb:set_properties({textures = {orig_txtr .. "_igniting2.png^[colorize:orange:100" .. col}})

      core.after(0.1, function()
        if not self then return end
        bomb:set_properties({visual_size = {x=6, y=6}, textures = {orig_txtr .. "_igniting3.png" .. col .. "^[brighten"}})

        -- questo non dovrebbe vedersi praticamente mai, ma giusto nel caso
        core.after(0.1, function()
          if not self then return end
          bomb:set_properties({visual_size = {x=6.5, y=6.5}, textures = {orig_txtr .. "_igniting4.png" .. col .. "^[brighten"}})
        end)
      end)
    end)
  end)
end