local S = core.get_translator("block_league")
local dmg1 = 15



local function register_peacifier(name, def)
  if def.variant_of then
    table.insert(core.registered_items[def.variant_of]._variants, name)
  end

  weapons_lib.register_weapon(name, {
    description = S("Peacifier"),
    _profile_description = S("Annoy your enemies from the distance"),
    _variant_name = def.variant_name,
    _variant_of = def.variant_of,
    _variants = {},
    groups = {bl_weapon_peacifier = 1},

    mesh = "bl_peacifier.obj",
    tiles = def.textures,
    wield_scale = {x=1.4, y=1.4, z=1.4},
    inventory_image = def.icon,
    use_texture_alpha = "clip",

    weapon_type = "gun",
    magazine = 30,
    reload_time = 2,
    slow_down_user = true,

    crosshair = "bl_peacifier_crosshair.png",
    sound_reload = "bl_peacifier_reload",

    can_use_weapon = function(player, action)
        return block_league.util_weapon_use_check(player)
    end,

    can_alter_speed = function(player)
      return block_league.util_on_can_alter_speed(player:get_player_name())
    end,

    on_reload = function(player, weapon)
      block_league.util_on_reload(player:get_player_name(), weapon.name)
    end,

    on_reload_end = function(player, weapon)
      block_league.util_on_reload_end(player:get_player_name(), weapon.name)
    end,

    on_after_hit = function(hitter, weapon, action, objects_hit, total_damage)
      block_league.util_on_after_hit(hitter, weapon, objects_hit, total_damage)
    end,

    action1 = {
      type = "raycast",
      _description = S("shot, @1♥", "<style color=#f66c77>" .. dmg1),
      damage = dmg1,
      range = 100,
      delay = 0.2,

      continuous_fire = true,

      sound = "bl_peacifier_shoot",
      trail = {
        image = "bl_peacifier_trail.png",
        amount = 5,
        size = 2,
      },

      on_use = function(player, weapon, action)
        block_league.util_update_mag_hud(player:get_player_name(), weapon.name)
      end,

      on_hit = function(hitter, target, weapon, action, hit_point, damage)
        return block_league.util_on_hit(hitter, target, weapon, action, hit_point, damage)
      end,
    },

    action2 = {
        type = "zoom",
        _description = S("zoom"),
        fov = 28,
    }
  })
end



register_peacifier("block_league:peacifier", {
  icon = "bl_peacifier.png",
  textures = {"bl_peacifier_texture.png"},
})

register_peacifier("block_league:peacifier_silver", {
  icon = "bl_peacifier_silver.png",
  textures = {"bl_peacifier_texture_silver.png"},
  variant_name = S("Silver"),
  variant_of = "block_league:peacifier"
})

register_peacifier("block_league:peacifier_gold", {
  icon = "bl_peacifier_gold.png",
  textures = {"bl_peacifier_texture_gold.png"},
  variant_name = S("Gold"),
  variant_of = "block_league:peacifier"
})

register_peacifier("block_league:peacifier_diamond", {
  icon = "bl_peacifier_diamond.png",
  textures = {"bl_peacifier_texture_diamond.png"},
  variant_name = S("Diamond"),
  variant_of = "block_league:peacifier"
})