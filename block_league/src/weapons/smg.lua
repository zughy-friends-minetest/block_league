local S = core.get_translator("block_league")
local dmg1 = 12
local dmg2 = 24
local ammo2 = 3

local function register_smg(name, def)
  if def.variant_of then
    table.insert(core.registered_items[def.variant_of]._variants, name)
  end

  weapons_lib.register_weapon(name, {
    description = S("Submachine Gun"),
    _profile_description = S("Your go-to weapon for close combat"),
    _variant_name = def.variant_name,
    _variant_of = def.variant_of,
    _variants = {},
    groups = {bl_weapon_mesh = 1},

    mesh = "bl_smg.obj",
    tiles = def.textures,
    wield_scale = {x=1.34, y=1.34, z=1.34},
    inventory_image = def.icon,
    use_texture_alpha = "clip",

    weapon_type = "gun",
    magazine = 30,
    reload_time = 2,
    slow_down_user = true,

    crosshair = "bl_smg_crosshair.png",
    sound_reload = "bl_smg_reload",

    can_use_weapon = function(player, action)
      return block_league.util_weapon_use_check(player)
    end,

    can_alter_speed = function(player)
      return block_league.util_on_can_alter_speed(player:get_player_name())
    end,

    on_reload = function(player, weapon)
      block_league.util_on_reload(player:get_player_name(), weapon.name)
    end,

    on_reload_end = function(player, weapon)
      block_league.util_on_reload_end(player:get_player_name(), weapon.name)
    end,

    on_after_hit = function(hitter, weapon, action, objects_hit, total_damage)
      block_league.util_on_after_hit(hitter, weapon, objects_hit, total_damage)
    end,

    action1 = {
      type = "raycast",
      _description = S("shot, decrease damage with distance, @1♥", "<style color=#f66c77>" .. dmg1),
      damage = dmg1,
      range = 30,
      delay = 0.1,
      --fire_spread = 0.2,

      decrease_damage_with_distance = true,
      continuous_fire = true,

      sound = "bl_smg_shoot",
      trail = {
        image = "bl_smg_trail.png",
        amount = 5
      },

      on_use = function(player, weapon, action)
        block_league.util_update_mag_hud(player:get_player_name(), weapon.name)
      end,

      on_hit = function(hitter, target, weapon, action, hit_point, damage)
        return block_league.util_on_hit(hitter, target, weapon, action, hit_point, damage)
      end,
    },

    action2 = {
      type = "raycast",
      _description = S("slower steadier shot with less reach, @1♥ @2►", "<style color=#f66c77>" .. dmg2, "<style color=#abc0c0>" .. ammo2),
      damage = dmg2,
      range = 20,
      delay = 0.5,
      ammo_per_use = ammo2,
      --TODO: booleano per far critici o meno?

      continuous_fire = true,

      sound = "bl_smg_shoot2",
      trail = {
        image = "bl_smg_trail2.png",
        amount = 10
      },

      on_use = function(player, weapon, action)
        block_league.util_update_mag_hud(player:get_player_name(), weapon.name)
      end,

      on_hit = function(hitter, target, weapon, action, damage, hit_point)
        return block_league.util_on_hit(hitter, target, weapon, action, damage, hit_point)
      end
    }
  })
end



register_smg("block_league:smg", {
  icon = "bl_smg.png",
  textures = {"bl_smg_texture.png"},
})

register_smg("block_league:smg_silver", { -- TODO: per l'exp fare `contains` OPPURE usare un parametro `_base` al posto di controllare nome esatto, così da includere varianti
  icon = "bl_smg_silver.png",
  textures = {"bl_smg_texture_silver.png"},
  variant_name = S("Silver"),
  variant_of = "block_league:smg"
})

register_smg("block_league:smg_gold", {
  icon = "bl_smg_gold.png",
  textures = {"bl_smg_texture_gold.png"},
  variant_name = S("Gold"),
  variant_of = "block_league:smg"
})

register_smg("block_league:smg_diamond", {
  icon = "bl_smg_diamond.png",
  textures = {"bl_smg_texture_diamond.png"},
  variant_name = S("Diamond"),
  variant_of = "block_league:smg"
})