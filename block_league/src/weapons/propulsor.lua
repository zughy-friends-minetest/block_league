local function can_use() end
local function dash() end



local function register_propulsor(name, desc, stamina)
  weapons_lib.register_weapon("block_league:" .. name, {
    description = desc,
    wield_scale = {x=1.3, y=1.3, z=1.3},
    wield_image = "bl_" .. name .. ".png",
    inventory_image = "bl_" .. name .. ".png",
    groups = {oddly_breakable_by_hand = "2", propulsor = 1},
    touch_interaction = "short_dig_long_place", -- jump with short tap

    weapon_type = "melee",
    crosshair = "bl_propulsor_crosshair.png",
    slow_down_user = false,

    can_use_weapon = function(player, action)
      return can_use(player)
    end,

    can_alter_speed = function(player)
      return block_league.util_on_can_alter_speed(player:get_player_name())
    end,

    action1 = {
      type = "custom",
      delay = 0.3,

      on_use = function(player, weapon, action, pointed_thing)
        -- se non sta puntando nulla o sta puntando un giocatore, annullo
        if pointed_thing.type == "nothing" or (pointed_thing.type == "object" and core.is_player(pointed_thing.ref)) then return end

        local p_name = player:get_player_name()
        local arena = arena_lib.get_arena_by_player(p_name)

        if arena then
          -- se non ha abbastanza energia, annullo
          if not (arena.players[p_name].stamina >= stamina) then return end
          arena.players[p_name].stamina = arena.players[p_name].stamina - stamina
        end

        local dir = player:get_look_dir()
        local knockback = player:get_velocity().y < 1 and -15 or -10

        player:add_velocity(vector.multiply(dir, knockback))
        weapons_lib.play_sound("bl_propulsor_bounce", p_name)
      end
    },

    action2 = {
      type = "custom",
      delay = 0.3,

      on_use = function(player, weapon, action, pointed_thing)
        dash(player, stamina)
      end
    }
  })
end



register_propulsor("propulsor", "Propulsor", 20)
register_propulsor("propulsor_dm", "Deathmatch Propulsor", 50)





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function dash(player, stamina)
  local p_name = player:get_player_name()
  local arena = arena_lib.get_arena_by_player(p_name)

  if arena then
    -- se non ha abbastanza energia, annullo
    if not (arena.players[p_name].stamina >= stamina) then return end
    arena.players[p_name].stamina = arena.players[p_name].stamina - stamina
  end

  local look_horizontal = player:get_look_horizontal()
  local rotate_factor = player:get_player_control().left and 1.57 or -1.57
  local dash_dir = vector.rotate_around_axis(core.yaw_to_dir(look_horizontal), {x=0,y=1,z=0}, rotate_factor)

  player:add_velocity(vector.multiply(dash_dir, 16))
  weapons_lib.play_sound("bl_sword_dash", p_name)
end



function can_use(player)
  local meta = player:get_meta()
  if player:get_hp() <= 0 or meta:get_int("wl_is_speed_locked") == 1 then return end

  block_league.remove_immunity(player)
  return true
end
