local S = core.get_translator("block_league")
local dmg = 80



local function register_pixelgun(name, def)
  if def.variant_of then
    table.insert(core.registered_items[def.variant_of]._variants, name)
  end

  weapons_lib.register_weapon(name, {
    description = S("Pixelgun"),
    _profile_description = S("Sniping weapon: you'll never be too far away"),
    _variant_name = def.variant_name,
    _variant_of = def.variant_of,
    _variants = {},
    groups = {bl_weapon_mesh = 1},

    mesh = "bl_pixelgun.obj",
    tiles = def.textures,
    wield_scale = {x=1.3, y=1.3, z=1.3},
    inventory_image = def.icon,
    use_texture_alpha = "clip",

    weapon_type = "snipe",
    magazine = 4,
    reload_time = 4,
    slow_down_user = true,

    crosshair = "bl_pixelgun_crosshair.png",
    sound_reload = "bl_pixelgun_reload",

    can_use_weapon = function(player, action)
      return block_league.util_weapon_use_check(player)
    end,

    can_alter_speed = function(player)
      return block_league.util_on_can_alter_speed(player:get_player_name())
    end,

    on_reload = function(player, weapon)
      block_league.util_on_reload(player:get_player_name(), weapon.name)
    end,

    on_reload_end = function(player, weapon)
      block_league.util_on_reload_end(player:get_player_name(), weapon.name)
    end,

    on_after_hit = function(hitter, weapon, action, objects_hit, total_damage)
      block_league.util_on_after_hit(hitter, weapon, objects_hit, total_damage)
    end,

    action1 = {
      type = "raycast",
      _description = S("piercing shot, @1♥", "<style color=#f66c77>" .. dmg),
      damage = dmg,
      range = 150,
      delay = 0.9,
      --load_time = 2,

      --attack_on_release = true, -- TODO: da implementare. Usa loading_time. Serve anche https://github.com/minetest/minetest/issues/13581
      pierce = true,

      sound = "bl_pixelgun_shoot",
      trail = {
        image = "bl_pixelgun_trail.png",
        amount = 20,
      },

      on_use = function(player, weapon, action)
        block_league.util_update_mag_hud(player:get_player_name(), weapon.name)
      end,

      on_hit = function(hitter, target, weapon, action, hit_point, damage)
        return block_league.util_on_hit(hitter, target, weapon, action, hit_point, damage)
      end,
    },

    action2 = {
      type = "zoom",
      _description = S("zoom"),
      fov = 20,
    }
  })
end



register_pixelgun("block_league:pixelgun", {
  icon = "bl_pixelgun.png",
  textures = {"bl_pixelgun_texture.png"},
})

register_pixelgun("block_league:pixelgun_silver", {
  icon = "bl_pixelgun_silver.png",
  textures = {"bl_pixelgun_texture_silver.png"},
  variant_name = S("Silver"),
  variant_of = "block_league:pixelgun"
})

register_pixelgun("block_league:pixelgun_gold", {
  icon = "bl_pixelgun_gold.png",
  textures = {"bl_pixelgun_texture_gold.png"},
  variant_name = S("Gold"),
  variant_of = "block_league:pixelgun"
})

register_pixelgun("block_league:pixelgun_diamond", {
  icon = "bl_pixelgun_diamond.png",
  textures = {"bl_pixelgun_texture_diamond.png"},
  variant_name = S("Diamond"),
  variant_of = "block_league:pixelgun"
})