arena_lib.on_respawn("block_league", function(arena, p_name)
  weapons_lib.HUD_crosshair_show(p_name)
  panel_lib.get_panel(p_name, "bl_skill"):show()
  panel_lib.get_panel(p_name, "bl_stamina"):show()
  panel_lib.get_panel(p_name, "bl_weapons"):show()
  panel_lib.get_panel(p_name, "bl_info_panel"):hide()

  local player = core.get_player_by_name(p_name)

  player:set_detach() -- in caso di giocanti. L'entità si rimuove in automatico quando non ha più figlɜ
  player:set_eye_offset()
  player:hud_set_flags({healthbar = true, hotbar = true, wielditem = true})
  player:set_properties({visual_size = {x = 1, y = 1, z = 1}})

  arena_lib.HUD_hide("hotbar", p_name)
  block_league.HUD_spectate_update(arena, p_name, "alive")

  local teamID = arena.players[p_name].teamID
  local random_spawner_id = math.random(1, arena_lib.get_arena_spawners_count(arena, teamID))

  block_league.add_immunity(player)
  arena_lib.teleport_onto_spawner(player, arena, random_spawner_id)

  -- TEMP: https://github.com/minetest/minetest/issues/12092 (anche random_spawner_id
  -- sopra. Usato per evitare che lə giocante venga buttatə in due posti diversi con
  -- eventuale sfarfallamento)
  core.after(0, function()
    arena_lib.teleport_onto_spawner(player, arena, random_spawner_id)
  end)

  core.after(0, function()
    local player_ = core.get_player_by_name(p_name)
    if not player_ then return end
    player_:get_meta():set_int("bl_invincibility", 0)
  end)

  arena.players[p_name].stamina = arena.players[p_name].stamina_max
  block_league.HUD_stamina_update(arena, p_name)
  block_league.reset_weapons_and_physics(player)
  block_league.refill_weapons(arena, p_name)

  local p_inv = player:get_inventory()

  -- TEMP: waiting for https://github.com/luanti-org/luanti/issues/15728 to get fixed
  p_inv:remove_item("main", ItemStack("air"))
  p_inv:add_item("main", ItemStack("air"))

  local teammates = arena_lib.get_players_in_team(arena, teamID, true)
  local teammates_alive = 0

  -- se è l'unicə giocante in vita della sua squadra (con squadra fatta da almeno 2 persone),
  -- fai che queste riseguano lə giocante appena tornatə in vita
  for _, pl in ipairs(teammates) do
    if pl:get_hp() > 0 then
      teammates_alive = teammates_alive + 1
    end
  end

  if teammates_alive == 1 then
    for _, pl in ipairs(teammates) do
      if pl:get_player_name() ~= p_name then
        block_league.death_spectate(pl, arena)
      end
    end
  end
end)