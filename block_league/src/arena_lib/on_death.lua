local S = core.get_translator("block_league")

local function wait_for_respawn() end



arena_lib.on_death("block_league", function(arena, p_name, reason)
  arena_lib.sound_play(p_name, "bl_death") -- se sono morto, non mi importa sentire anche quello dellɜ compagnɜ di squadra

  local player = core.get_player_by_name(p_name)

  -- Stando a git blame, penso fosse stato messo per le entità che vengono invocate
  -- per immobilizzare chi gioca (es. per l'attacco in salto con la spada)
  if player:get_attach() then
    player:get_attach():remove()
  end

  -- Se era seguitə da compagnɜ mortɜ, riassegnalɜ
  block_league.reassign_dead_teammates(player, arena)

  local p_stats = arena.players[p_name]

  block_league.remove_player_entities(p_stats.entities)

  -- TD: se il giocatore è morto con la palla, questa si sgancia e torna a oscillare
  if arena.mode == 1 then
    local ball = block_league.get_ball(player)

    if ball then
      if player:get_pos().y < arena.min_y then
        ball:reset()
      else
        ball:detach()
      end

      -- reindirizza sulla palla gli spettatori
      for sp_name, _ in pairs(arena_lib.get_player_spectators(p_name)) do
        if arena.spectators[sp_name].was_following_ball then
          arena_lib.spectate_target("block_league", arena, sp_name, "entity", "Ball")
        end
      end
    end

  -- DM: se muoio suicida, perdo un'uccisione
  elseif arena.mode == 2 then
    p_stats.kills = p_stats.kills - 1
    local team_id = p_stats.teamID
    local team = arena.teams[team_id]
    team.deaths = team.deaths + 1
    block_league.HUD_infopanel_update_points(arena, team_id)
  end

  local p_pos = player:get_pos()
  local staticdata = {rot = {x=0, y=player:get_look_horizontal(), z=0}, txtr = player:get_properties().textures[1]}

  player:set_pos(vector.new(p_pos.x, p_pos.y + 1.6, p_pos.z))
  player:set_properties({visual_size = {x = 0, y = 0, z = 0}})
  player:hud_set_flags({healthbar = false, hotbar = false, wielditem = false})

  player:get_meta():set_int("bl_invincibility", 1)
  core.add_entity(p_pos, "block_league:corpse", core.serialize(staticdata))

  -- TODO: mostra pannello con info di chi ti ha ucciso
  weapons_lib.HUD_crosshair_hide(p_name)
  panel_lib.get_panel(p_name, "bl_skill"):hide()
  panel_lib.get_panel(p_name, "bl_stamina"):hide()
  panel_lib.get_panel(p_name, "bl_weapons"):hide()
  panel_lib.get_panel(p_name, "bl_info_panel"):show() -- TEMP: https://github.com/minetest/minetest/issues/15663

  block_league.death_spectate(player, arena)
  wait_for_respawn(arena, p_name, arena.death_waiting_time)
end)





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function wait_for_respawn(arena, p_name, time_left)
  if not arena_lib.is_player_in_arena(p_name, "block_league") or arena.weapons_disabled then
    arena_lib.HUD_hide("broadcast", p_name)
    return end

  if time_left > 0 then
    arena_lib.HUD_send_msg("broadcast", p_name, S("Back in the game in @1", time_left))
  else
    local player = core.get_player_by_name(p_name)

    player:respawn()
    arena_lib.HUD_hide("broadcast", p_name)
    return
  end

  time_left = time_left -1

  core.after(1, function()
    wait_for_respawn(arena, p_name, time_left)
  end)
end