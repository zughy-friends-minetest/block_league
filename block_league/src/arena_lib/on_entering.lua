local S = core.get_translator("block_league")

local function reset_meta() end
local function init_dmg_table() end
local function equip() end
local function create_and_show_HUD() end



arena_lib.on_load("block_league", function(arena)
  local players = arena.players

  for pl_name, stats in pairs(players) do
    reset_meta(pl_name)
    init_dmg_table(pl_name, players)
    block_league.reset_weapons_and_physics(core.get_player_by_name(pl_name))
    equip(arena, pl_name)
    create_and_show_HUD(arena, pl_name)
    block_league.HUD_keys_create(pl_name)  -- a parte perché non la voglio nell'on_join. Poi rimossa su on_start

    core.get_player_by_name(pl_name):get_meta():set_int("bl_invincibility", 1)

    if arena.mode ~= 0 then
      block_league.refill_weapons(arena, pl_name)
    end

    stats.entering_time = arena.initial_time
  end

  if arena.mode ~= 0 then
    core.after(0.1, function()
      block_league.HUD_infopanel_update_points_all(arena)
    end)

    arena_lib.HUD_send_msg_all("broadcast", arena, S("The game will start soon"))
    block_league.countdown_and_start(arena, 3)

  else
    for pl_name, _ in pairs(players) do
      bl_tutorial.initialise(arena, pl_name)
    end
  end

  block_league.stamina_refill_loop(arena)
end)



arena_lib.on_join("block_league", function(p_name, arena, as_spectator, was_spectator)
  if as_spectator then
    create_and_show_HUD(arena, p_name, true)
    return
  end

  local players = arena.players

  players[p_name].entering_time = arena.current_time

  reset_meta(p_name)
  init_dmg_table(p_name, players, true)
  weapons_lib.reset_state(core.get_player_by_name(p_name))
  equip(arena, p_name)
  create_and_show_HUD(arena, p_name, false, was_spectator)
  block_league.HUD_spectate_addplayer(arena, p_name)
  block_league.refill_weapons(arena, p_name)

  block_league.play_sound_to_player(p_name, "bl_voice_fight")

  core.after(0.1, function()
    block_league.HUD_infopanel_update_points_all(arena)
    block_league.HUD_scoreboard_update_score(arena)
  end)
end)





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function reset_meta(p_name)
  local p_meta = core.get_player_by_name(p_name):get_meta()

  p_meta:set_int("bl_has_ball", 0)
  p_meta:set_int("bl_immunity", 0)
  p_meta:set_int("bl_invincibility", 0)
end



function init_dmg_table(p_name, players, in_progress)
  local dmg_table = players[p_name].dmg_received

  -- potrebbero esserci armi con fuoco amico, metti qualsiasi giocante
  for pl_name, pl_data in pairs(players) do
    dmg_table[pl_name] = {timestamp = 99999, dmg = 0}

    for ent_name, _ in pairs(pl_data.entities) do
      dmg_table[pl_name .. "@" .. ent_name] = {timestamp = 99999, dmg = 0}
    end
  end

  -- se in corso, aggiungo nuovə giocante per chi era già dentro
  if in_progress then
    for _, pl_data in pairs(players) do
      pl_data.dmg_received[p_name] = {timestamp = 99999, dmg = 0}

      for _, ent_obj in pairs(pl_data.entities) do
        if ent_obj:get_luaentity() then -- mi fido 0
          ent_obj:get_luaentity()._dmg_received[p_name] = {timestamp = 99999, dmg = 0}
        end
      end
    end
  end
end



function equip(arena, p_name)
  local inv = core.get_player_by_name(p_name):get_inventory()

  if arena.mode == 0 then
    inv:add_item("main", ItemStack("block_league:smg")) -- per creare l'HUD

  else
    local weapons = block_league.get_equipped_weapons(p_name)
    local propulsor = arena.mode == 1 and "block_league:propulsor" or "block_league:propulsor_dm"

    for i = 1, 3 do
      if weapons[i] then
        inv:set_stack("main", i, ItemStack(weapons[i]))
      end
    end
    inv:set_stack("main", 4, ItemStack(propulsor))

    local skill = block_league.get_equipped_skill(p_name)

    p_name:enable_skill(skill)
  end
end



function create_and_show_HUD(arena, p_name, is_spectator, was_spectator)
  -- se stava già seguendo come spettatorə
  if was_spectator then
    panel_lib.get_panel(p_name, "bl_weapons"):remove()
    panel_lib.get_panel(p_name, "bl_skill"):remove()
    block_league.HUD_spectate_remove(arena.players, p_name)

    core.after(0.1, function()
      block_league.HUD_scoreboard_update_score(arena)
      block_league.HUD_infopanel_update_spectators(arena)
    end)

    local team_marker = arena.players[p_name].teamID == 1 and "bl_hud_scoreboard_orangemark.png" or "bl_hud_scoreboard_bluemark.png"
    panel_lib.get_panel(p_name, "bl_scoreboard"):update(nil, nil, {team_marker = {text = team_marker}})
    block_league.HUD_stamina_update(arena, p_name)

  -- se entra per la prima volta
  else
    block_league.HUD_stamina_create(p_name)
    block_league.HUD_broadcast_create(p_name)
    block_league.HUD_log_create(p_name)
    block_league.HUD_scoreboard_create(arena, p_name, is_spectator)
  end

  block_league.HUD_critical_create(p_name) -- TODO: abbastanza sicuro che questo non debba essere generato ogni volta
  block_league.HUD_weapons_create(p_name, is_spectator, arena.mode)
  block_league.HUD_skill_create(p_name, is_spectator)

  if is_spectator then
    block_league.HUD_spectate_create(arena, p_name)
    core.after(0.1, function() block_league.HUD_infopanel_update_spectators(arena) end)
  else
    block_league.HUD_infopanel_create(p_name)
    block_league.HUD_crosshair_create(p_name)
    weapons_lib.HUD_crosshair_show(p_name)
    block_league.add_hp_bar(arena, core.get_player_by_name(p_name))
  end
end