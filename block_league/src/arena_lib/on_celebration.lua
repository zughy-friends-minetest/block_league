arena_lib.on_celebration("block_league", function(arena, winners)
  arena.weapons_disabled = true

  for pl_name, _ in pairs(arena.players) do
    local player = core.get_player_by_name(pl_name)

    weapons_lib.deactivate_zoom(player)
    player:get_meta():set_int("bl_immunity", 1)

    panel_lib.get_panel(pl_name, "bl_info_panel"):show()
    arena_lib.HUD_hide("broadcast", pl_name)
    audio_lib.stop_sound(pl_name, "bl_voice_countdown_3") -- in case of people quitting before the match starts

    -- se son mortɜ, tieni nomi nascosti o fluttuano attorno alle persone
    if player:get_hp() <= 0 then
      player:set_nametag_attributes({color = {a = 0, r = 255, g = 255, b = 255}})
    end
  end

  if winners then
    local losers = winners == 1 and 2 or 1

    arena_lib.sound_play_team(arena, winners, "bl_jingle_victory")
    arena_lib.sound_play_team(arena, losers, "bl_jingle_defeat")
  else
    arena_lib.sound_play_all(arena, "bl_jingle_victory")
  end
end)
