local S = core.get_translator("block_league")



arena_lib.on_prejoin("block_league", function(arena, p_name, as_spectator)
  if as_spectator and arena.mode == 0 then
    core.chat_send_player(p_name, core.colorize("#e6482e", S("[!] Tutorial can't be spectated!")))
    return
  else
    return true
  end
end)