local S = core.get_translator("block_league")

arena_lib.on_enable("block_league", function(arena, p_name)
  if not arena.teams_enabled and (arena.mode == 1 or arena.mode == 2) then
    arena_lib.print_error(p_name, S("This mode forcibly requires teams to work!"))
    return end

  if arena.teams_enabled and arena.mode == 0 then
    arena_lib.print_error(p_name, S("This mode forcibly requires no teams to work!"))
    return end

  if arena.mode == 1 then
    if not next(arena.ball_spawn) then
      arena_lib.print_error(p_name, S("You haven't set the ball spawner!"))
      return end

    if not next(arena.goal_blue) or not next(arena.goal_orange) then
      arena_lib.print_error(p_name, S("You haven't set the goals!"))
      return end
  end

  return true
end)