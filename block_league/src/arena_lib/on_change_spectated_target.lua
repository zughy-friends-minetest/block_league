arena_lib.on_change_spectated_target("block_league", function(arena, sp_name, t_type, t_name, prev_type, prev_spectated, is_forced)
  local sp_data = arena.spectators[sp_name]

  if t_type == "player" then
    if is_forced and prev_type == "entity" then
        sp_data.was_following_ball = true
    elseif not is_forced and sp_data.was_following_ball then
        sp_data.was_following_ball = false
    end

    -- se stava seguendo la palla, rimostra le HUD
    if not panel_lib.get_panel(sp_name, "bl_stamina"):is_visible() then
      panel_lib.get_panel(sp_name, "bl_stamina"):show()
      panel_lib.get_panel(sp_name, "bl_weapons"):show()
      core.get_player_by_name(sp_name):hud_set_flags({healthbar = true})
    end

    local p_weaps = block_league.get_equipped_weapons(t_name)

    for i = 1, 3 do
      if p_weaps[i] then
        block_league.HUD_weapons_update(arena, t_name, p_weaps[i])
      else
        block_league.HUD_weapons_update_empty(t_name, i)
      end
    end

    block_league.HUD_skill_update(sp_name)
    block_league.HUD_stamina_update(arena, t_name)

  elseif t_type == "entity" then
    -- se al seguire la palla questa è in testa a qualcunə, segui quel qualcunə
    local parent = arena_lib.get_spectatable_entities("block_league", arena.name)[t_name].object:get_attach()

    if not is_forced and not sp_data.was_following_ball and parent then
      arena_lib.spectate_target("block_league", arena, sp_name, "player", parent:get_player_name())
      sp_data.was_following_ball = true

    -- sennò nascondi le varie HUD
    else
      panel_lib.get_panel(sp_name, "bl_stamina"):hide()
      panel_lib.get_panel(sp_name, "bl_weapons"):hide()
      core.get_player_by_name(sp_name):hud_set_flags({healthbar = false})
    end

  elseif t_type == "area" then
    if is_forced and prev_type == "entity" then
        sp_data.was_following_ball = true
    elseif not is_forced and sp_data.was_following_ball then
        sp_data.was_following_ball = false
    end
  end
end)