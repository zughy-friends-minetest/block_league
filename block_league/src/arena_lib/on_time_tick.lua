arena_lib.on_time_tick("block_league", function(arena)
    block_league.HUD_scoreboard_update_time(arena)

    if arena.current_time <= 10 then
        arena_lib.sound_play_all(arena, "bl_timer")
    end
end)