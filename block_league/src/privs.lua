local S = core.get_translator("block_league")

core.register_privilege("blockleague_admin", {
  description = S("It allows to use the /bladmin command")
})

core.register_privilege("blockleague_betatester", {
  description = ":eyes:"
})
