local function display_and_start_countdown() end
local function round_start() end
local function load_ball() end



function block_league.countdown_and_start(arena, time)
  core.after(3, function()
    if arena.in_celebration then return end
    arena_lib.sound_play_all(arena, "bl_voice_countdown_3")
    display_and_start_countdown(arena, time)
  end)
end



function block_league.refill_weapons(arena, p_name)
  local player = core.get_player_by_name(p_name)

  for i, w_name in pairs(block_league.get_equipped_weapons(p_name)) do
    weapons_lib.refill(p_name, player:get_inventory():get_stack("main", i))
    block_league.HUD_weapons_update(arena, p_name, w_name)
  end
end



function block_league.reset_weapons_and_physics(player)
  weapons_lib.deactivate_zoom(player)
  weapons_lib.reset_state(player)
  player:set_physics_override(block_league.PHYSICS)
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function display_and_start_countdown(arena, time_left)
  if time_left > 0 then
    arena_lib.HUD_send_msg_all("broadcast", arena, time_left)
    time_left = time_left -1

    core.after(1, function()
      if arena.in_celebration then return end
      display_and_start_countdown(arena, time_left)
    end)

  else
    arena_lib.HUD_hide("broadcast", arena)
    round_start(arena)
  end
end



function round_start(arena)
  for pl_name, pl_data in pairs(arena.players) do
    local player = core.get_player_by_name(pl_name)

    if player:get_hp() <= 0 then
      player:respawn()
    else
      player:set_hp(999)
      arena.players[pl_name].stamina = arena.players[pl_name].stamina_max
      block_league.HUD_stamina_update(arena, pl_name)
      block_league.reset_weapons_and_physics(player)
      block_league.refill_weapons(arena, pl_name)
      block_league.remove_player_entities(pl_data.entities)
      arena_lib.teleport_onto_spawner(player, arena)
    end

    block_league.remove_immunity(player)
  end

  -- rendi impossibile uccidere lɜ giocanti nel primo step (raggi, bombe)
  core.after(0, function()
    for pl_name, _ in pairs(arena.players) do
      core.get_player_by_name(pl_name):get_meta():set_int("bl_invincibility", 0)
    end
  end)

  arena_lib.sound_play_all(arena, "bl_voice_fight")
  block_league.HUD_log_clear(arena)

  if arena.mode == 1 then
    load_ball(arena)
  end

  arena.weapons_disabled = false
end



function load_ball(arena)
  core.forceload_block(arena.ball_spawn, true)
  core.add_entity(arena.ball_spawn,"block_league:ball",arena.name)

  for sp_name, sp_data in pairs(arena.spectators) do
    if sp_data.was_following_ball then
      arena_lib.spectate_target("block_league", arena, sp_name, "entity", "Ball")
    end
  end
end
