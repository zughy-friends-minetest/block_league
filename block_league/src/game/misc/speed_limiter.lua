local max_vel = 16.5
local smoothness = 1.5

-- to avoid triggering huge propulsor jumps, that may happen due to issues with
-- physics (it seems to happen more online than locally, maybe packet issues, no idea)
core.register_globalstep(function ()
  for _, player in ipairs(arena_lib.get_players_in_minigame("block_league", true)) do
    local pl_y = player:get_velocity().y
    if pl_y > max_vel then
      local diff = pl_y - max_vel
      player:add_velocity({x=0, y=-diff/smoothness, z=0})
    end
  end
end)