local S = core.get_translator("block_league")

local function countdown() end

local old_textures = {}                 -- KEY: p_name; VALUE: {textures}



function block_league.add_immunity(player)
  local p_meta = player:get_meta()

  if p_meta:get_int("bl_immunity") == 1 then return end

  p_meta:set_int("bl_immunity", 1)

  local p_name = player:get_player_name()
  local textures = player:get_properties().textures

  old_textures[p_name] = player:get_properties().textures

  for k, v in ipairs(textures) do
    textures[k] = v .. "^[multiply:#7d7071"
  end

  player:set_properties({textures = textures})

  local arena = arena_lib.get_arena_by_player(p_name)

  countdown(arena, player, arena.immunity_time)
end



function block_league.remove_immunity(player)
  local p_meta = player:get_meta()

  if p_meta:get_int("bl_immunity") == 0 then return end

  local p_name = player:get_player_name()

  arena_lib.HUD_hide("broadcast", p_name)
  p_meta:set_int("bl_immunity", 0)
  player:set_properties({textures = old_textures[p_name]})
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function countdown(arena, player, seconds_left)
  local p_name = player:get_player_name()

  arena_lib.HUD_send_msg("broadcast", p_name, S("Immunity off in: @1", seconds_left))

  if seconds_left == 0 then
    block_league.remove_immunity(player)
  else
    core.after(1, function()
      if not arena.players[p_name] or arena.in_celebration or player:get_meta():get_int("bl_immunity") == 0 then return end

      countdown(arena, player, seconds_left -1)
    end)
  end
end
