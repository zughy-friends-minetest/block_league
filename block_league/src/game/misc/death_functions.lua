local S = core.get_translator("block_league")

local function show_kill_text() end
local function increase_points() end
local function assist() end
local function update_hud() end
local function end_if_dm() end
local function deserialize_dmgr() end



function block_league.kill_by_suicide(arena, player, dmg_rcvd_table, no_hitter_img)
  block_league.remove_immunity(player)
  player:set_hp(0)

  -- se le armi son disabilitate (quindi o in caricamento o dopo punto o in celebrazione) non mostrare niente
  if arena.weapons_disabled then
    block_league.HUD_spectate_update(arena, player:get_player_name(), "alive")
    return end

  local last_hitter = ""
  local last_hitter_timestamp = 99999
  local last_hitter_icon = ""

  for dmgr_name, dmg_data in pairs(dmg_rcvd_table) do
    if arena.current_time > dmg_data.timestamp - 5 and last_hitter_timestamp > dmg_data.timestamp then
      last_hitter = dmgr_name
      last_hitter_timestamp = dmg_data.timestamp
      last_hitter_icon = dmg_data.w_icon
    end
  end

  if last_hitter ~= "" then
    if last_hitter:find("@") then
      local ent_name, owner_name = deserialize_dmgr(last_hitter)
      local entity = arena.players[owner_name].entities[ent_name]:get_luaentity()

      block_league.entity_kill_player(arena, entity, player)
    else
      block_league.kill_player(arena, last_hitter_icon, core.get_player_by_name(last_hitter), player)
    end
  else
    local p_name = player:get_player_name()
    block_league.HUD_spectate_update(arena, p_name, "alive")
    block_league.HUD_log_update(arena, no_hitter_img, p_name, "")
  end
end



function block_league.kill_player(arena, log_img, player, target)
  local p_name = player:get_player_name()
  local t_name = target:get_player_name()

  -- riproduco suono morte e aggiorno avatar per spettatorɜ
  block_league.play_sound_to_player(p_name, "bl_kill")
  block_league.HUD_spectate_update(arena, t_name, "alive")

  if t_name ~= p_name then
    show_kill_text(p_name, t_name)

    local p_stats = arena.players[p_name]
    local team_id = p_stats.teamID
    local team = arena.teams[team_id]

    -- TODO: metti 0 punti se uccidi gente della tua squadra
    local points = increase_points(arena, player, target, p_stats, team)
    local a_name = assist(arena, p_name, t_name, points)

    update_hud(arena, p_name, t_name, a_name, team_id, log_img)
    end_if_dm(arena, team, team_id, p_name)

  else
    block_league.HUD_kill_update(t_name, S("You've killed yourself"))
    block_league.HUD_log_update(arena, "bl_log_suicide.png", p_name, "")
  end
end



function block_league.kill_entity(arena, log_img, player, target)
  local entity_lua = target:get_luaentity()
  local p_name = player:get_player_name()
  local t_name = entity_lua._name

  block_league.play_sound_to_player(p_name, "bl_kill")
  block_league.HUD_kill_update(p_name, S("YOU'VE KILLED @1", t_name))

  if arena_lib.is_player_spectated(p_name) then
    for sp_name, _ in pairs(arena_lib.get_player_spectators(p_name)) do
      block_league.HUD_kill_update(sp_name, S("@1 HAS KILLED @2", p_name, t_name))
    end
  end

  local p_stats = arena.players[p_name]
  local team_id = p_stats.teamID

  p_stats.points = p_stats.points + 2

  local a_name = assist(arena, p_name, entity_lua, 2)

  update_hud(arena, p_name, entity_lua, a_name, team_id, log_img)
end



function block_league.entity_kill_player(arena, entity, target)
  local p_name = entity._owner
  local t_name = target:get_player_name()

  block_league.play_sound_to_player(p_name, "bl_kill")
  block_league.HUD_spectate_update(arena, t_name, "alive")

  show_kill_text(p_name, t_name)

  local p_stats = arena.players[p_name]
  local team_id = p_stats.teamID
  local team = arena.teams[team_id]

  local points = increase_points(arena, core.get_player_by_name(p_name), target, p_stats, team)
  local a_name = assist(arena, p_name .. "@" .. entity._name, t_name, points)

  update_hud(arena, p_name, t_name, a_name, team_id, entity._log_icon, entity)
  end_if_dm(arena, team, team_id, p_name)
end



function block_league.entity_kill_entity(arena, entity, target)
  local p_name = entity._owner
  local t_name = target._name

  show_kill_text(p_name, t_name)

  local p_stats = arena.players[p_name]
  local team_id = p_stats.teamID

  p_stats.points = p_stats.points + 2

  local a_name = assist(arena, p_name .. "@" .. entity._name, target, 2)

  block_league.play_sound_to_player(p_name, "bl_kill")
  update_hud(arena, p_name, target, a_name, team_id, entity._log_icon, entity)
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function show_kill_text(p_name, t_name)
  block_league.HUD_kill_update(p_name, S("YOU'VE KILLED @1", t_name))
  core.chat_send_player(t_name, core.colorize("#d7ded7", S("You've been killed by @1", core.colorize("#eea160", p_name))))

  if arena_lib.is_player_spectated(p_name) then
    for sp_name, _ in pairs(arena_lib.get_player_spectators(p_name)) do
      block_league.HUD_kill_update(sp_name, S("@1 HAS KILLED @2", p_name, t_name))
    end
  end

  if arena_lib.is_player_spectated(t_name) then
    for sp_name, _ in pairs(arena_lib.get_player_spectators(t_name)) do
      core.chat_send_player(sp_name, core.colorize("#d7ded7", S("@1 has been killed by @2", core.colorize("#eea160", t_name), core.colorize("#eea160", p_name))))
    end
  end
end



function increase_points(arena, player, target, p_stats, team)
  local points

  -- aggiungo l'uccisione
  team.kills = team.kills + 1
  p_stats.kills = p_stats.kills + 1

  -- calcolo i punti
  if arena.mode == 1 then
    if player:get_meta():get_int("bl_has_ball") == 1 or (target:is_player() and target:get_meta():get_int("bl_has_ball") == 1) then
      points = 4
    else
      points = 2
    end
  else
    points = 2
  end

  p_stats.points = p_stats.points + points
  return points
end




-- target, tabella = entità, stringa = gioc
function assist(arena, atkr_name, t_lua_or_t_name, points)
  local is_target_player = type(t_lua_or_t_name) == "string"
  local t_name, dmg_table

  if is_target_player then
    t_name = t_lua_or_t_name
    dmg_table = arena.players[t_name].dmg_received
  else
    t_name = t_lua_or_t_name._name
    dmg_table = t_lua_or_t_name._dmg_received
  end

  local a_dmg = 0
  local a_name, a_name_owner

  -- controlla per assist..
  for dmgr_name, data in pairs(dmg_table) do
    local dmg = data.dmg
    if dmgr_name ~= atkr_name and arena.current_time > data.timestamp - 5 and dmg > 5 and dmg > a_dmg then
      a_name = dmgr_name
      a_dmg = dmg
    end
  end

  if not a_name then return end

  a_name, a_name_owner = deserialize_dmgr(a_name)
  local entity_marker = a_name_owner and "@" -- dì a HUD_log_update(..) che potrebbe essere un'entità

  -- potrebbe non essere più in arena
  if arena.players[a_name_owner] then
    block_league.play_sound_to_player(a_name_owner, "bl_kill")
    block_league.HUD_kill_update(a_name_owner, S("YOU'VE CONTRIBUTED TO KILL @1", t_name))

    if arena_lib.is_player_spectated(a_name_owner) then
      for sp_name, _ in pairs(arena_lib.get_player_spectators(a_name_owner)) do
        block_league.HUD_kill_update(sp_name, S("@1 HAS CONTRIBUTED TO KILL @2", a_name, t_name))
      end
    end

    arena.players[a_name_owner].points = arena.players[a_name_owner].points + (points / 2)
    block_league.HUD_spectate_update(arena, a_name_owner, "points")
  end

  return entity_marker .. a_name
end



function update_hud(arena, p_name, t_lua_or_t_name, a_name, team_id, log_img, entity)
  block_league.HUD_infopanel_update_points(arena, team_id)
  block_league.HUD_spectate_update(arena, p_name, "points")
  block_league.HUD_log_update(arena, log_img, entity or p_name, t_lua_or_t_name, a_name)
end



function end_if_dm(arena, team, team_id, p_name)
  if arena.mode == 2 then
    block_league.HUD_scoreboard_update_score(arena)
    if team.kills == arena.score_cap then
      local mod = arena_lib.get_mod_by_player(p_name)
      arena_lib.load_celebration(mod, arena, team_id)
    end
  end
end



function deserialize_dmgr(a_name)
  local a_name_owner
  local is_entity = string.find(a_name, "@")

  -- in caso di entità proprietà di giocanti
  if is_entity then
    a_name_owner = a_name:match("^[^@]+")
    a_name = a_name:sub(is_entity + 1)
  else
    a_name_owner = a_name
  end

  return a_name, a_name_owner
end