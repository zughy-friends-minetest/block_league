block_league.SPEED = 2.5
block_league.SHOOT_SPEED_MULTIPLIER = 0.6
block_league.SPEED_LOW = block_league.SPEED * block_league.SHOOT_SPEED_MULTIPLIER

block_league.PHYSICS = {
  speed = block_league.SPEED,
  jump = 1.5,
  gravity = 1.15,
  sneak_glitch = true,
  new_move = true
}