(WIP)

## Mechanics
### Critical hits
Hitting players to their head with a raycast attack results in a critical hit (it can't be done with e.g. melee attacks for now, due to LT limitations).  

The formula is
```
critical_damage = damage * 1.5
```

### Immunity
To avoid the practice of spawnkilling, players are immune for 3 seconds when they respawn. This is shown through a dark patina applied onto the character model + a text HUD sent to the player, displaying how many seconds until the immunity goes off.  

If a player shoots whilst being immune, immunity is removed before time.  
When a new round starts, nobody is immune. Meaning that if a player has died before the beginning of a new round and remained dead until then, it won't be immune when respawning.

## Player Metadata
As a general rule, every player metadata is set to `0` when entering the arena.

* `bl_immunity`: (int) whether the player can be damaged by other players
  * set to `0` after a few seconds after respawning. Shooting and taking the ball instantly set it to 0
  * set to `1` when respawning and when the celebration phase starts
* `bl_invincibility`: (int) whether the player can be damaged in general (i.e. rays). It's an extra layer of safety to prevent damage when respawning
  * set to `0` one step after players respawn and rounds start
  * set to `1` when dying and when the arena loads

## Custom Properties
Block League weapons, actions and entities use specific fields, which are all `nil` by default

### Weapon properties

* `_profile_description`: (string) the description that appears when selecting the weapon in the Block League profile
* `_variant_name`: (string) the name of the variant, if any
* `_variant_of`: (string) the technical name of the base weapon, if any
* `_variants`: (table) in base weapons, a list of strings containing the technical names of all the variants registered for that weapon

#### Action properties

* `_description`: (string) describes the action when selecting the weapon in the Block League profile
* `_friendly_fire`: (boolean) whether it should hurt your team (including yourself) as well
* `_friendly_dmg_perc`: (float) how much damage should deal in percentage, in case `_friendly_fire` is `true`. Range `0-1`

### Entity properties
Shared:

* `_teamID`: (int) the ID of the team the entity belongs to

#### Stand-alone weapons
(As for now, sentry guns)

* `_name`: (string) the name of the entity. Used to display the entity in the kill log
* `_loggable`: (boolean) whether the entity, once destroyed, shall appear in the kill log
* `_owner`: (string) who owns the entity, e.g. sentry guns
* `_can_be_targeted`: (boolean) whether the entity can be hit by stand-alone weapons such as sentries
* `_no_knockback`: (boolean) whether the weapon is subject to weapons' knocback. It requires `_can_be_targeted`
* `_dmg_received`: (table) tracks the damage dealt by every player and targettable entity, same as in the arena player property with the same name. Format `[p_name/p_name@ent._name] = {dmg, timestamp}`
* `_kill`: (function(self, killer)) custom actions to run when the entity is killed
* `_is_dying`: (boolean) whether the entity is dying. Used by `_kill`, hp are set to 1 in meanwhile

Each entity can then feature its unique fields

* Sentry guns: `_is_spawning`, `_arena`

#### Bullets

Unique fields

* Bombs: `_collided`, `_ignited`

## API
* `block_league.get_equipped_weapons(p_name)`: returns a list containing the technical names of the weapons `p_name` has currently equipped
* `block_league.get_equipped_skill(p_name)`: returns the technical name of the skill `p_name` has currently equipped
* `block_league.has_weapon_equipped(p_name, w_name, <check_for_variants>)`: whether `p_name` has `w_name` currently equipped. if `check_for_variants` is `true`, it'll check whether the base weapon is the same
* `block_league.has_variant(p_name, w_name, var_name)`: whether `p_name` has unlocked `var_name`
* `block_league.get_unlocked_weapon_variants(p_name, w_name)`: returns a table containing all the variants of `w_name` that `p_name` has unlocked

## Guidelines
### Audio
Block League uses 4 different functions to play audio files to a single player, each one with its specific usecase
* `block_league.play_sound_to_player(p_name, sound)`: plays the sound to `p_name` and to whomever is attached onto them - both spectatos and dead players
* `weapons_lib.play_sound(sound, p_name)`: like `play_sound_to_player`, but to keep weapons_lib convention is should be used only in weapons custom actions (e.g. Propulsor)
* `arena_lib.sound_play(p_name, sound, <override_params>)`: plays the sound to `p_name` and their spectators. It skips dead players
* `audio_lib.play_sound(sound, {to_player = p_name})`: ignores whatever spectator and dead player `p_name` might have (used for the tutorial)
