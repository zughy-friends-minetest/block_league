# Block League

Main mod. For a general README see [here]((README.md))

### Dependencies
* [achievements_lib](https://content.luanti.org/packages/Zughy/achievements_lib/) by me
* [arena_lib](https://content.luanti.org/packages/Zughy/arena_lib/) by me
* (bundled by arena_lib) [Chat Command Builder](https://content.luanti.org/packages/rubenwardy/lib_chatcmdbuilder/) by rubenwardy
* [controls](https://content.luanti.org/packages/BuckarooBanzay/controls/) by Arcelmi/BuckarooBanzay
* [panel_lib](https://content.luanti.org/packages/Zughy/panel_lib/) by me
* [skills](https://content.luanti.org/packages/giov4/skills/) by Giov4
* (optional) [Visible Wielditem](https://content.luanti.org/packages/LMD/visible_wielditem/) by appguru

### Set up an arena
1. run `/arenas create block_league <arena_name>`
2. enter the editor via `/arenas edit <arena_name>`
3. have fun customising

### Utility commands
* `/bladmin removevar <p_name> weapon <w_name> <var_name>`: removes variant `var_name` of weapon `w_name` from `p_name`
* `/bladmin testkit`: gives you the propulsor and the in-game physics, to easily test your maps. The last object in the hotbar restores your inventory and physics
* `/bladmin unlockvar <p_name> weapon <w_name> <var_name>`: unlocks variant `var_name` of weapon `w_name` for `p_name`
