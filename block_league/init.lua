local version = "0.12.0-dev"
local modpath = core.get_modpath("block_league")
local srcpath = modpath .. "/src"

local S = core.get_translator("block_league")
local NS = function(s) return s end

block_league = {}
dofile(modpath .. "/GLOBALS.lua")
dofile(srcpath .. "/_load.lua")


arena_lib.register_minigame("block_league", {
  name = S("Block League"),
  prefix = "[" .. S("Block League") .. "] ",
  icon = "bl_pixelgun.png",

  teams = { S("orange"), S("blue") },
  teams_color_overlay = { "orange", "blue"},
  can_disable_teams = true,     -- for tutorial only
  friendly_fire = true,         -- bombs

  player_aspect = {visual_size = {x = 1, y = 1, z = 1}}, -- evita che uscendo da mortɜ, rimangano invisibili
  camera_offset = {
    nil,
    {x=8, y=4, z=-1}
  },

  hp_max = 100,
  hotbar = {
    slots = 4,
    background_image = "bl_gui_hotbar.png"
  },

  load_time = 6,
  celebration_time = 5,
  join_while_in_progress = true,
  time_mode = "decremental",

  can_drop = false,
  disable_inventory = true,
  disabled_damage_types = {"fall"},
  in_game_physics = block_league.PHYSICS,
  hud_flags = {
    crosshair = false,
    healthbar = true,
    hotbar = true,
    minimap = false,
    wielditem = true
  },

  custom_messages = {
    celebration_nobody = NS("Tie!")
  },

  properties = {
    mode = 1,           -- 0 Tutorial, 1 TD, 2 DM
    score_cap = 5,
    immunity_time = 5,
    death_waiting_time = 6,
    goal_orange = {},
    goal_blue = {},
    ball_spawn = {},
    min_y = 0
  },
  temp_properties = {
    weapons_disabled = true,
  },
  team_properties = {
    TDs = 0,
    kills = 0,
    deaths = 0
  },
  player_properties = {
    stamina = 100,
    stamina_max = 100,
    TDs = 0,
    kills = 0,
    points = 0,
    --entering_time = 0,            -- inutilizzato, servirà prob in futuro per calcolare exp
    current_weapon = "",
    dmg_received = {},              -- KEY: dmgr_name, VALUE: {timestamp, dmg, w_icon (questa solo x armi)}. Se è entità, p_name@ent_name -- TODO: in futuro usa UUID
    dmg_dealt = 0,                  -- memorizza il danno inflitto ai nemici per il conteggio dei punti
    entities = {}                   -- KEY: _name, VALUE: ObjRef -- TEMP: in futuro usare UUID https://github.com/minetest/minetest/pull/14135
  },
  spectator_properties = {
    was_following_ball = false
  }
})



-- general
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/database_manager.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/privs.lua")
dofile(srcpath .. "/utils.lua")

-- visible_wielditem
dofile(srcpath .. "/deps/visible_wielditem.lua")
-- arena_lib
dofile(srcpath .. "/arena_lib/arena_editor.lua")
dofile(srcpath .. "/arena_lib/on_celebration.lua")
dofile(srcpath .. "/arena_lib/on_change_spectated_target.lua")
dofile(srcpath .. "/arena_lib/on_death.lua")
dofile(srcpath .. "/arena_lib/on_enable.lua")
dofile(srcpath .. "/arena_lib/on_entering.lua")
dofile(srcpath .. "/arena_lib/on_leaving.lua")
dofile(srcpath .. "/arena_lib/on_prejoin.lua")
dofile(srcpath .. "/arena_lib/on_respawn.lua")
dofile(srcpath .. "/arena_lib/on_start.lua")
dofile(srcpath .. "/arena_lib/on_time_tick.lua")
dofile(srcpath .. "/arena_lib/on_timeout.lua")
-- debug
dofile(srcpath .. "/debug/testkit.lua")
-- GUI
dofile(srcpath .. "/GUI/gui_profile.lua")
-- HUD
dofile(srcpath .. "/HUD/hud_broadcast.lua")
dofile(srcpath .. "/HUD/hud_critical.lua")
dofile(srcpath .. "/HUD/hud_crosshair.lua")
dofile(srcpath .. "/HUD/hud_info_panel.lua")
dofile(srcpath .. "/HUD/hud_keys.lua")
dofile(srcpath .. "/HUD/hud_log.lua")
dofile(srcpath .. "/HUD/hud_medals.lua")
dofile(srcpath .. "/HUD/hud_scoreboard.lua")
dofile(srcpath .. "/HUD/hud_skill.lua")
dofile(srcpath .. "/HUD/hud_spectate.lua")
dofile(srcpath .. "/HUD/hud_stamina.lua")
dofile(srcpath .. "/HUD/hud_weapons.lua")
-- game
dofile(srcpath .. "/game/game_main.lua")
dofile(srcpath .. "/game/input_manager.lua")
dofile(srcpath .. "/game/misc/death_functions.lua")
dofile(srcpath .. "/game/misc/death_state.lua")
dofile(srcpath .. "/game/misc/fall.lua")
dofile(srcpath .. "/game/misc/health_bar.lua")
dofile(srcpath .. "/game/misc/immunity.lua")
dofile(srcpath .. "/game/misc/speed_limiter.lua")
dofile(srcpath .. "/game/misc/stamina.lua")
dofile(srcpath .. "/game/TD/ball.lua")
dofile(srcpath .. "/game/TD/rays.lua")
-- player
dofile(srcpath .. "/player/achievements.lua")
dofile(srcpath .. "/player/equip.lua")
dofile(srcpath .. "/player/exp.lua")
-- skills
dofile(srcpath .. "/skills/hp+.lua")
dofile(srcpath .. "/skills/sp+.lua")
-- abstract weapons
dofile(srcpath .. "/weapons/weapons_utils.lua")
-- weapons
dofile(srcpath .. "/weapons/bomb.lua")
dofile(srcpath .. "/weapons/peacifier.lua")
dofile(srcpath .. "/weapons/pixelgun.lua")
dofile(srcpath .. "/weapons/propulsor.lua")
dofile(srcpath .. "/weapons/sentry_gun.lua")
dofile(srcpath .. "/weapons/smg.lua")
dofile(srcpath .. "/weapons/sword.lua")
dofile(srcpath .. "/weapons/entities/sentrygun_entity.lua")
--dofile(srcpath .. "/weapons/junkyard/kunai.lua")
--dofile(srcpath .. "/weapons/junkyard/rocket_launcher.lua")

core.log("action", "[BLOCK_LEAGUE] Mod initialised, running version " .. version)
