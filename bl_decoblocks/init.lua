local srcpath = core.get_modpath("bl_decoblocks") .. "/src"

bl_decoblocks = {}

dofile(srcpath .. "/maps/hyperium.lua")
dofile(srcpath .. "/maps/neden1.lua")
dofile(srcpath .. "/maps/tunnel.lua")
dofile(srcpath .. "/concrete.lua")
dofile(srcpath .. "/utils.lua")