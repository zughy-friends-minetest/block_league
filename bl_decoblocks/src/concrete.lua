local S = core.get_translator("bl_decoblocks")
local NS = function(s) return s end


local function register_concrete(name, desc)
  name = "concrete_" .. name
  desc = "[BL] " .. S(desc)

  core.register_node("bl_decoblocks:" .. name, {
  	description = desc,
  	tiles = {"bldecoblocks_" .. name .. ".png"},
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults()
  })

  core.register_node("bl_decoblocks:" .. name .. "_slab", {
  	description = desc .. " " .. S("(slab)"),
    drawtype = "nodebox",
    paramtype = "light",
    paramtype2 = "facedir",
  	tiles = {"bldecoblocks_" .. name .. ".png"},
    node_box = {
  		type = "fixed",
  		fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
    },
    groups = {cracky = 3},
    sounds = default.node_sound_stone_defaults(),

    on_place = function(itemstack, placer, pointed_thing)
      bl_decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
    end,
  })

  core.register_node("bl_decoblocks:" .. name .. "_stair", {
		description = desc .. " " .. S("(stair)"),
		drawtype = "nodebox",
		paramtype = "light",
		paramtype2 = "facedir",
    tiles = {"bldecoblocks_" .. name .. ".png"},
    node_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, 0.0, 0.5},
				{-0.5, 0.0, 0.0, 0.5, 0.5, 0.5},
			},
		},
		groups = {cracky = 3},
		sounds = default.node_sound_stone_defaults(),

		on_place = function(itemstack, placer, pointed_thing)
			if pointed_thing.type ~= "node" then
				return itemstack
			end

			return bl_decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
		end,
	})

  core.register_alias(name, "bl_decoblocks:" .. name)
  core.register_alias(name .. "_slab", "bl_decoblocks:" .. name .. "_slab")
  core.register_alias(name .. "_stair", "bl_decoblocks:" .. name .. "_stair")
end

register_concrete("black", NS("Black concrete"))
register_concrete("blue", NS("Blue concrete"))
register_concrete("dark_grey", NS("Dark grey concrete"))
register_concrete("grey", NS("Grey concrete"))
register_concrete("light_grey", NS("Light grey concrete"))
register_concrete("red", NS("Red concrete"))


