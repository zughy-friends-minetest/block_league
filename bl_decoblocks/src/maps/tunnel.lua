local function register_tunnel_block(n)
	core.register_node("bl_decoblocks:blockleague_tunnel_" .. n, {
    description = "[BL] Tunnel " .. n,
    drawtype = "nodebox",
    tiles = {
      "bldecoblocks_tunnel_" .. n .. ".png",
      "bldecoblocks_tunnel_" .. n .. ".png",
      "bldecoblocks_concrete_light_grey.png"
    },
    node_box = {
      type = "fixed",
      fixed = {-0.5, -0.25, -0.5, 0.5, 0.25, 0.5}
    },
    paramtype = "light",
    paramtype2 = "facedir",
    light_source = default.LIGHT_MAX/3,
    groups = {oddly_breakable_by_hand = 2},
    sounds = default.node_sound_stone_defaults(),

    on_place = function(itemstack, placer, pointed_thing)
      bl_decoblocks.place_and_rotate(itemstack, placer, pointed_thing)
    end,
  })
end

register_tunnel_block(1)
register_tunnel_block(2)
